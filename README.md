**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Description

Plugins wordpress permettant de gérer des questionnaires

1. Créer des questionnaires
2. Gérer les répondants
3. Collecter les réponses
4. Analyser les réponses collecter

---

## Dépendance

Ce plugins nécessite l'installation des plugins suivantes :

1. WP MVC

## HTACCESS
---------------------------start----------------------------------------
<IfModule mod_rewrite.c>
RewriteEngine On
RewriteBase /apter/
RewriteRule ^index\.php$ - [L]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . /apter/index.php [L]
</IfModule>


<IfModule mod_rewrite.c>
RewriteEngine On
RewriteBase /apter/
RewriteRule ^index\.php$ - [L]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . /apter/index.php [L]
</IfModule>


---------------------------end---------------------------------------------