<?php

class ApterSurveyLoader extends MvcPluginLoader {

    var $db_version = '1.0';
    var $tables = array();

    function activate() {
    
        // This call needs to be made to activate this app within WP MVC
        
        $this->activate_app(__FILE__);
        
        // Perform any databases modifications related to plugin activation here, if necessary

        require_once ABSPATH.'wp-admin/includes/upgrade.php';
    
        add_option('apter_survey_db_version', $this->db_version);
        
        // Use dbDelta() to create the tables for the app here
        // $sql = '';
        // dbDelta($sql);
        global $wpdb;
       
        $sql = '
            CREATE TABLE IF NOT EXISTS '.$wpdb->prefix.'apter_ccs (
                id int(11) NOT NULL AUTO_INCREMENT,
                nom varchar(255),
                prenom varchar(255),
                phone varchar(255),
                organisation varchar(255) NOT NULL,
                date_add datetime ,
                add_by int(11) DEFAULT NULL,
                password varchar(255) DEFAULT NULL,
                statut varchar(255),
                email varchar(255),
                PRIMARY KEY (id)
            )
            ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

        ';
        dbDelta($sql);
        

        $sql = '
            CREATE TABLE IF NOT EXISTS '.$wpdb->prefix.'apter_questionnaires (
                id int(11) NOT NULL AUTO_INCREMENT,
                type varchar(255) NOT NULL,
                date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                date_update timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                nom varchar(255),
                nb_destinataire int DEFAULT 0,
                nb_resultat int DEFAULT 0,
                consultant_id int,
                ben_civilite varchar(100),
                ben_firstname varchar(100),
                ben_lastname varchar(100),
                ben_societe varchar(100),
                ben_initiales varchar(100),
                ben_email varchar(100),
                ben_tel varchar(100),
                ben_langue varchar(100),
                ben_code varchar(255),
                send TINYINT(1) DEFAULT 0,
                reponse TEXT,
             
                statut TINYINT(1) DEFAULT 1,
                collecte TINYINT(1) DEFAULT 0,

                PRIMARY KEY (id)
            )
            ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

        ';
        dbDelta($sql);

        $sql = '
                CREATE TABLE IF NOT EXISTS '.$wpdb->prefix.'apter_pages(
                id int(11) NOT NULL AUTO_INCREMENT,
                apter_questionnaire_id INT not null,
                titre varchar(255),
                sous_titre varchar(255),
                description text,
                PRIMARY KEY (id),
                FOREIGN KEY (apter_questionnaire_id) REFERENCES '.$wpdb->prefix.'apter_questionnaires(id)
            )
            ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ';
        dbDelta($sql);

        $sql = '
                CREATE TABLE IF NOT EXISTS '.$wpdb->prefix.'apter_groupes(
                id int(11) NOT NULL AUTO_INCREMENT,
                apter_questionnaire_id INT not null,
                nom varchar(255) not null,
                abreviation varchar(50) not null,
                description text,
                PRIMARY KEY (id),
                FOREIGN KEY (apter_questionnaire_id) REFERENCES '.$wpdb->prefix.'apter_questionnaires(id)
            )
            ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ';
        dbDelta($sql);

        
        $sql = '
                CREATE TABLE IF NOT EXISTS '.$wpdb->prefix.'apter_repondants(
                id int(11) NOT NULL AUTO_INCREMENT,
                email varchar(255) NOT NULL,
                firstname varchar(255),
                lastname varchar(255),
                civilite varchar(100),
                apter_questionnaire_id int(11),
                apter_groupe_id int(11),
                code varchar(255),
                reponse TEXT,
                send TINYINT(1) DEFAULT 0,
                PRIMARY KEY (id),
                FOREIGN KEY (apter_questionnaire_id) REFERENCES '.$wpdb->prefix.'apter_questionnaires(id),
                FOREIGN KEY (apter_groupe_id) REFERENCES '.$wpdb->prefix.'apter_groupes(id)
            )
            ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;';
        dbDelta($sql);
        $sql = '
                 CREATE TABLE IF NOT EXISTS '.$wpdb->prefix.'apter_questions(
                id int(11) NOT NULL AUTO_INCREMENT,
                apter_questionnaire_id INT not null,
                question text NOT NULL,
                type VARCHAR(255) NOT NULL,
                apter_page_id INT(11) NOT NULL,
                requis TINYINT(1) DEFAULT 1,
                negative TINYINT(1) DEFAULT 0,
                PRIMARY KEY (id),
                FOREIGN KEY (apter_questionnaire_id) REFERENCES '.$wpdb->prefix.'apter_questionnaires(id),
                FOREIGN KEY (apter_page_id) REFERENCES '.$wpdb->prefix.'apter_pages(id)
            )
            ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ';
        dbDelta($sql);
        $sql = '
                CREATE TABLE IF NOT EXISTS '.$wpdb->prefix.'apter_reponses(
                id int(11) NOT NULL AUTO_INCREMENT,
                apter_question_id INT not null,
                reponse text not null,
                score INT DEFAULT 0,
                vote INT DEFAULT 0,
                PRIMARY KEY (id),
                FOREIGN KEY (apter_question_id) REFERENCES '.$wpdb->prefix.'apter_questions(id)
            )
            ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ';
        dbDelta($sql);

        $sql = '
                CREATE TABLE IF NOT EXISTS '.$wpdb->prefix.'apter_mails(
                id int(11) NOT NULL AUTO_INCREMENT,
                type varchar(255) not null,
                titre varchar(255) not null,
                mail text,
                PRIMARY KEY (id)
                
            )
            ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ';
        dbDelta($sql);

        

        $rows = array(
              array(
                  
                  'titre' => "Bénéficiaire",
                  'type' => 'beneficiaire',
                  'mail' => "<p>%BEN_CIVILITE%,</p><br>

                    <p>
                    Dans le cadre du développement des compétences managériales de votre entreprise, nous avons été mandatés pour réaliser des évaluations coaching 360 Feed Back Leadership et Management.
                    </p><br>
                    <p>
                        Cette démarche offre un effet de levier positif et pertinent sur le développement managérial des personnes qui en bénéficient.
                    </p>

                    <p>
                    Concrètement, elle vise à recueillir une évaluation de plusieurs sources (vous-même bien sûr, votre hiérarchie, ainsi que des personnes choisies par vous-même, parmi vos collègues, vos collaborateurs, vos clients), dans le cadre d’un processus performant qui vous donne l’opportunité d’effectuer un état des lieux sur vos compétences managériales et vos talents de leadership.
                    </p>
                    <p>
                    Le rapport 360° feedback Leadership et Management qui vous sera transmis vous permettra d’identifier votre potentiel managérial et d’élaborer un plan de progrès pertinent pour votre évolution. Il sera remis uniquement à vous-même. La garantie de confidentialité est une exigence de qualité afin d’obtenir des réponses non biaisées et obtenir un rapport avec des résultats tout à fait légitimes.  
                    </p><br>

                    <p>
                    Nous vous invitons à compléter la grille des répondants fournie en Pièce jointe et à nous la transmettre par mail.  
                    </p>
                    <p>
                    Une session de coaching individuelle vous sera proposée pour debriefer ce rapport et en tirer le plus grand bénéfice pour vous. Cette session pourra se faire, en fonction de votre agenda et du coach, en présentiel ou distanciel.    
                    </p>
                    <p>
                    Cette démarche est une réelle plus-value pour vous-même et nous espérons que vous y attacherez un bel intérêt. Nous sommes à votre disposition pour échanger plus avant avec vous et faire de ce moment, une réelle source de développement pour vous-même.
                    </p>
                    <br>
                    <p>Lien : %SITE_LIEN%</p>
                    <p>Code : <b>%SITE_CODE%</b></p>
                    <br>



                    Bien cordialement  <br>
                    %CONS_FIRSTNAME% %CONS_LASTNAME% <br>
                    %CONS_EMAIL% . <br>
                    Apter France <br>
                    "
                  
              ),
              array(
                  
                  'titre' => "Répondants",
                  'type' => 'repondants',
                  'mail' => "<p>%REP_CIVILITE%, </p>
                <br>
                <p>
                    Dans le cadre du développement managérial de %BEN_CIVILITE% %BEN_FIRSTNAME% %BEN_LASTNAME%, nous réalisons pour %BEN_SOCIETE%, une évaluation 360°Feed Back Leadership et Management.
                </p>
                <br>
                <p>Cette démarche a pour objectif d'offrir un effet de levier positif et pertinent sur les collaborateurs qui en bénéficient. Elle est une opportunité de dresser un état des lieux autant en termes de compétences que de talents managériaux. Vous avez été choisi par Madame Sanae Benkerroum pour participer à son évaluation.</p>
                <br>
                <p>
                    Notre déontologie ainsi que le process de collecte des données mis en place vous garantissent que vos réponses sont tout à fait confidentielles.
                </p>
                <p>Lien : %SITE_LIEN%</p>
                 <p>Code : <b>%SITE_CODE%</b></p>
                <p><br>
                Nous restons à votre disposition pour toute information complémentaire.
                </p>
                <br>
                Bien cordialement  <br>
                %CONS_FIRSTNAME% %CONS_LASTNAME% <br>
                %CONS_EMAIL% . <br>
                Apter France <br>
                "
                  
              ),
              array(
                  
                  'titre' => "Relance",
                  'type' => 'relance',
                  'mail' => "<p>%REP_CIVILITE%, </p><br>

                    <p>
                        Nous vous avons récemment contacté dans le cadre de l'évaluation 360 Feed Back Leadership et Management de %REP_CIVILITE% %REP_FIRSTNAME% %REP_LASTNAME% , mais sauf erreur de notre part, n'avons pas reçu vos réponses. Votre participation est importante pour le bénéficiaire de ce regard, qui vous a invité. 
                    </p>
                    <p>
                        Vos réponses sont tout à fait confidentielles.
                    </p><br>

                    <p>Cliquez sur le lien ci-dessous pour commencer ou poursuivre cette évaluation. </p>
                    <p>%SITE_LIEN%</p>
                    Code : <b>%SITE_CODE%</b>
                    <br>
                    <p>Nous vous remercions de votre attention et de votre disponibilité pour répondre à cette évaluation 360.</p>
                    <p>Nous restons à votre disposition pour toute information complémentaire.</p>

                    <br>
                    Bien cordialement  <br>
                    %CONS_FIRSTNAME% %CONS_LASTNAME% <br>
                    %CONS_EMAIL% . <br>
                    Apter France <br>
                    "
                  
              )
          );
        foreach($rows as $row) {
              $this->wpdb->insert($wpdb->prefix.'apter_mails', $row);
        }
        /*$this->wpdb->insert($wpdb->prefix.'apter_questionnaires',["type" => "360 FB Management et Leadership"]);
        */
    }

    function deactivate() {
    
        // This call needs to be made to deactivate this app within WP MVC
        
        $this->deactivate_app(__FILE__);
        
        // Perform any databases modifications related to plugin deactivation here, if necessary
        global $wpdb;

          // this needs to occur at this level, and not in the
          // constructor/init since we are switching blogs for multisite
        
         /*$sql = 'SET FOREIGN_KEY_CHECKS = 0;';
          $wpdb->query($sql);

          $sql = 'DROP TABLE '.$wpdb->prefix.'apter_questionnaires;';
          $wpdb->query($sql);

          $sql = 'DROP TABLE ' . $wpdb->prefix.'apter_pages;';
          $wpdb->query($sql);

          $sql = 'DROP TABLE ' . $wpdb->prefix.'apter_groupes;';
          $wpdb->query($sql);

          $sql = 'DROP TABLE ' . $wpdb->prefix.'apter_repondants;';
          $wpdb->query($sql);

          $sql = 'DROP TABLE ' . $wpdb->prefix.'apter_questions;';
          $wpdb->query($sql);

          $sql = 'DROP TABLE ' . $wpdb->prefix.'apter_reponses;';
          $wpdb->query($sql);*/

          $sql = 'DROP TABLE ' . $wpdb->prefix.'apter_mails;';
          $wpdb->query($sql);
         $sql = 'SET FOREIGN_KEY_CHECKS = 1;';
          $wpdb->query($sql);
          $sql = 'UPDATE '.$wpdb->prefix.'apter_reponses SET score=0 WHERE reponse="je ne sais pas";';
        $wpdb->query($sql);
          $sql = 'ALTER TABLE '.$wpdb->prefix.'apter_reponses ALTER COLUMN score INT DEFAULT 0;';
        $wpdb->query($sql);
    }

}

?>