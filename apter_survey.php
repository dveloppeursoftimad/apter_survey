<?php
/*
Plugin Name: Apter Survey
Plugin URI: 
Description: Plugin for creating Apter 360 Test
Author: Softimad
Version: 1.0.0
Author URI: 
*/

register_activation_hook(__FILE__, 'apter_survey_activate');
register_deactivation_hook(__FILE__, 'apter_survey_deactivate');

function apter_survey_activate() {
    global $wp_rewrite;
    require_once dirname(__FILE__).'/apter_survey_loader.php';
    $loader = new ApterSurveyLoader();
    $loader->activate();
    $wp_rewrite->flush_rules( true );
}

function apter_survey_deactivate() {
    global $wp_rewrite;
    require_once dirname(__FILE__).'/apter_survey_loader.php';
    $loader = new ApterSurveyLoader();
    $loader->deactivate();
    $wp_rewrite->flush_rules( true );
}

?>