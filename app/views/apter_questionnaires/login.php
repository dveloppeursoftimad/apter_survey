
<style type="">
	.error{
		color: red;
	}
	
</style>

<script type="text/javascript" src="<?php echo mvc_js_url("apter-survey","angular") ?>"></script>
<?php add_thickbox(); ?>


<div ng-app="apterApp" ng-controller="ApterController as apter" ng-cloak>
	<header class="entry-header">
		<h1 class="entry-title">Test 360 FB Management et Leadership</h1>	</header>
		<p class="error">
			{{ apter.error }}
		</p>

	<div class="entry-content" ng-show="apter.repondant">
		<form>
			<div>
				<label>Nom</label>
				<input type="text" ng-model='apter.lastname'>
			</div>
			<div>
				<label>Prénom</label>
				<input type="text" ng-model="apter.firstname">
			</div>
			<div>
				<label>Groupe</label>
				<select ng-model="apter.apter_groupe_id">
					<option ng-repeat="groupe in apter.groupes" value="{{groupe.id}}">{{ groupe.nom }}</option>
				</select>
			</div>
			<div>
				<label>Code</label>
				<input type="text" ng-model="apter.code">
			</div>
			<br>
			<div>
				<button ng-click="apter.validate()">Valider</button>
			</div>
			
		</form>

		
	</div>

	

</div>

<script type="text/javascript">
	var app = angular.module('apterApp',[])
		.controller('ApterController', function($http){
			var apter = this;
			apter.firstname = "";
			apter.lastname = "";
			apter.code = "";
			apter.apter_groupe_id = "";
			apter.code = "";
			apter.error = "";
			
			<?php
				if(isset($groupes)){
					?>
					apter.groupes = <?php echo json_encode($groupes) ?>;
					<?php
				}
			?>
			
			apter.validate = function(){
				$http({
					  method: 'POST',
					  url: ajaxurl,
					  data: {
					  	action : "admin_apter_questionnaires_ajax_validate_repondant",
					  	data : apter

					  }
					}).then(function successCallback(response) {
						apter.error= response.data.message;
						console.log(response);
						if(response.data.repondant){
							console.log(response.data.repondant);
							apter.repondant = response.data.repondant
						}
					    
					  }, function errorCallback(response) {
					    // called asynchronously if an error occurs
					    // or server returns response with an error status.
					  });
			}

			
		});

		app.config(function ($httpProvider, $httpParamSerializerJQLikeProvider){
		    $httpProvider.defaults.transformRequest.unshift($httpParamSerializerJQLikeProvider.$get());
		    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=utf-8';
		});


</script>