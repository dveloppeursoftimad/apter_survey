
<style type="">
	.error{
		color: red;
	}
		.loader {
	margin: 50px auto;
    border: 9px solid #f3f3f3; /* Light grey */
    border-top: 9px solid grey; /* Blue */
    border-radius: 50%;
    width: 60px;
    height: 60px;
    animation: spin 1s linear infinite;
}

@keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
}
	
</style>

<link rel="stylesheet" type="text/css" href="<?php echo mvc_css_url("apter-survey","bootstrap") ?>">
<script type="text/javascript" src="<?php echo mvc_js_url("apter-survey","bootstrap") ?>"></script>
<script type="text/javascript" src="<?php echo mvc_js_url("apter-survey","angular") ?>"></script>
<script type="text/javascript" src="<?php echo mvc_js_url("apter-survey","jquery") ?>"></script>
<?php add_thickbox(); ?>


<div ng-app="apterApp" ng-controller="ApterController as apter" ng-cloak>

		<a href="http://www.methode-apter.com/diagnostics"><img src="http://www.methode-apter.com/diagnostics/wp-content/themes/apter/images/logo-content.jpg" alt="Logo Apter-France" class="logo-content img-responsive" style="width: 30%;"></a>
	
		<div class="titre">{{ apter.ApterQuestionnaire.type }}</div>

		

		
		<p class="error">
			{{ apter.error }}
		</p>
	<div class="center" ng-show="apter.loading">
    	<div class="loader" ></div> 
    </div>

	<div class="entry-content" ng-show="!apter.repondant && !apter.loading">
		<div class="titre-infos">Merci d’entrer les informations manquantes</div>
		<form name='repondant-form'>
			<p>
				<label>Nom</label>
				<input type="text" ng-model='apter.lastname' name="lastname" >
				<p class="error">{{ apter.lastname_error }}</p>
			</p>
			<p>
				<label>Prénom</label>
				<input type="text" ng-model="apter.firstname" name="firstname" >
				<p class="error">{{ apter.firstname_error }}</p>
			</p>
			<p>
				<label>Groupe</label>

				<span class="select-input">
					<select ng-model="apter.apter_groupe_id" required>
						<option ng-repeat="groupe in apter.groupes" value="{{groupe.id}}">{{ groupe.nom }}</option>
					</select>
				</span>
				
				<p class="error">{{ apter.groupe_error }}</p>
			</p>
			<p>
				<label>Code</label>
				<input type="text" ng-model="apter.code" name="code">
				<p class="error">{{ apter.code_error }}</p>
			</p>
			<br>
			<p>
				<button class="btn-item active" ng-click="apter.validate()">Valider</button>
			</p>
			
		</form>

		
	</div>

	<span id="titre"></span>

	<div class="entry-content" ng-show="apter.repondant && !apter.loading">
		<div class="progress-bootstrap">
		  <div class="progress-bootstrap-bar progress-bootstrap-bar-warning" role="progressbar" aria-valuenow="{{ apter.progress }}"
		  aria-valuemin="0" aria-valuemax="100" style="width:{{ apter.progress }}%">
		    {{ apter.progress }}%
		  </div>
		</div>

		<div  ng-repeat='page in apter.ApterQuestionnaire.apter_pages' ng-show="apter.ApterQuestionnaire.apter_pages[$index]['active']">
			<h2 ng-show="page.titre">{{ page.titre }}</h2>
			<h3 ng-show="page.sous_titre">{{ page.sous_titre }}</h3>
			<hr>
				
			
				<div ng-repeat="question in page.apter_questions">
					<h4>{{ question.question }}</h4>
					<div class="error">
						{{ question.error_message }}
					</div>
						<div ng-repeat="reponse in question.apter_reponses">
							<label ng-show="question.type =='radio'">
								<input type="radio" ng-model="question.reponse" name="question-{{$parent.$parent.$index}}-{{$parent.$index}}" value="{{reponse.score}}"> <span style="font-weight: normal;">{{ reponse.reponse}} </span>
							</label>
							
							 
						</div>
						<input type="text" name="" ng-show="question.type == 'text'" ng-model="question.reponse_text">


		
					
					<br>
					
				</div>
				<button class="btn-item active" ng-click="apter.next(page)">{{ apter.page_button }}</button>
			
			
		</div>
			

	</div>

	<div class="entry-content" ng-show="apter.save_result">
		<p>Vos réponses ont été sauvegardées.</p>
		<p>Nous vous remercions d'avoir participé a ce test.</p>
		
	</div>

</div>

<script type="text/javascript">
	var app = angular.module('apterApp',[])
		.controller('ApterController', function($http){
			var apter = this;
			apter.firstname = "";
			apter.lastname = "";
			apter.code = "";
			apter.apter_groupe_id = "";
			apter.code = "";
			apter.error = "";
			apter.page_cur = 0;
			apter.page_button = "Suivant";
			apter.save_result = false;
			apter.loading = false;
			apter.progress= 0;
			apter.nb_pages = 0;
			apter.nb_pages_visited = 0;

			<?php
				if(isset($objects)){
					?>
					apter.ApterQuestionnaire = <?php echo json_encode($objects) ?>;
					console.log(apter.ApterQuestionnaire.apter_pages.length);
					apter.nb_pages = apter.ApterQuestionnaire.apter_pages.length;
					if(apter.ApterQuestionnaire.apter_pages.length > 0){
						apter.ApterQuestionnaire.apter_pages[apter.page_cur]['active'] = true;
					} 
					/*for (var i = apter.ApterQuestionnaire.apter_pages.length - 1; i >= 0; i--) {
						for (var j = apter.ApterQuestionnaire.apter_pages[i].apter_questions.length - 1; j >= 0; j--) {
							apter.ApterQuestionnaire.apter_pages[i].apter_questions[j].reponse = "";
						};
						
					}*/
					
					if(apter.ApterQuestionnaire.apter_pages.length == 1){
						apter.page_button = "Valider";
					}
					<?php
				}
			?>

			<?php
				if(isset($groupes)){
					?>
					apter.groupes = <?php echo json_encode($groupes) ?>;
					<?php
				}
			?>
			<?php
				if(isset($id)){
					?>
					apter.id = <?php echo json_encode($id) ?>;
					<?php
				}
			?>
			apter.validate = function(){
				if(apter.validate_repondant()){
					apter.loading = true;
					var data = {
						apter_groupe_id : apter.apter_groupe_id,
						code : apter.code,
						id : apter.id,
						firstname : apter.firstname,
						lastname : apter.lastname,
					};
					
					$http({
						  method: 'POST',
						  url: ajaxurl,
						  data: {
						  	action : "admin_apter_questionnaires_ajax_validate_repondant",
						  	data : data

						  }
						}).then(function successCallback(response) {
							apter.loading = false;
							apter.error= response.data.message;
							console.log(response);
							if(response.data.repondant){

								console.log(response.data.repondant);
								apter.repondant = response.data.repondant
							}
						    
						  }, function errorCallback(response) {
						    // called asynchronously if an error occurs
						    // or server returns response with an error status.
						  });
				}
					
			}

			apter.validate_repondant = function(){
				var state = true;
				console.log(apter.lastname);
				if(!apter.lastname){
					apter.lastname_error = "Le nom est requis";
					state = false;

				}
				if(!apter.firstname){
					apter.firstname_error = "Le prénom est requis";
					state = false;
				}
				if(!apter.apter_groupe_id){
					apter.groupe_error = "Le groupe est requis";
					state = false;
				}
				if(!apter.code){
					apter.code_error = "Le code est requis";
					state = false;
				}
				return state;
			}

			apter.next = function(page){
				$('html,body').animate({ scrollTop: 0 }, 'slow');
				if(apter.validateForm(page)){
					apter.nb_pages_visited +=1;
					apter.progress =parseInt((100 *apter.nb_pages_visited)/apter.nb_pages);
					apter.ApterQuestionnaire.apter_pages[apter.page_cur]['active'] = false;
					if(angular.isDefined(apter.ApterQuestionnaire.apter_pages[apter.page_cur+1])){
						apter.page_cur = apter.page_cur +1;
						apter.ApterQuestionnaire.apter_pages[apter.page_cur]['active'] = true;
					}else{
						apter.loading = true;
						console.log(apter.ApterQuestionnaire);
						apter.Result = [];
						for(var i in apter.ApterQuestionnaire.apter_pages){
							for(var j in apter.ApterQuestionnaire.apter_pages[i].apter_questions){
									if(apter.ApterQuestionnaire.apter_pages[i].apter_questions[j].negative == 1){
										if(apter.ApterQuestionnaire.apter_pages[i].apter_questions[j].reponse){
											if(apter.ApterQuestionnaire.apter_pages[i].apter_questions[j].reponse != 0){
												apter.ApterQuestionnaire.apter_pages[i].apter_questions[j].reponse = 5-apter.ApterQuestionnaire.apter_pages[i].apter_questions[j].reponse;
											}
											
										}
										
									}
									var result = {
										question_id: apter.ApterQuestionnaire.apter_pages[i].apter_questions[j].id,
										score: apter.ApterQuestionnaire.apter_pages[i].apter_questions[j].reponse,
										groupe : apter.repondant.apter_groupe.abreviation,
										page_id : apter.ApterQuestionnaire.apter_pages[i].id,
										type : apter.ApterQuestionnaire.apter_pages[i].apter_questions[j].type,
										text : apter.ApterQuestionnaire.apter_pages[i].apter_questions[j].reponse_text
									}
									apter.Result.push(result);

							}
						}
						console.log(apter.Result);
						$http({
						  method: 'POST',
						  url: ajaxurl,
						  data: {
						  	action : "admin_apter_questionnaires_ajax_add_result",
						  	data : {
						  		reponse : JSON.stringify(apter.Result),
						  		repondant_id: apter.repondant.id,
						  		id: apter.id
						  	}

						  }
						}).then(function successCallback(response) {
							apter.save_result = true;
							apter.loading = false;
							console.log(response);
							
						    
						  }, function errorCallback(response) {
						    // called asynchronously if an error occurs
						    // or server returns response with an error status.
						  });
						
					}
					if(angular.isDefined(apter.ApterQuestionnaire.apter_pages[apter.page_cur+1])){

					}else{
						apter.page_button = "Valider";
					}
				}else{
					page.error_message = "Veullez bien remplir le questionnaire";
				}
				
					
				
			}

			apter.validateForm = function(page){
				for(var i in page.apter_questions){
					if(angular.isUndefined(page.apter_questions[i].reponse) && angular.isUndefined(page.apter_questions[i].reponse_text)){
						page.apter_questions[i].error_message = "* Veuillez répondre à cette question";
						return false;
					}
				}
				return true;
			}

		});

		app.config(function ($httpProvider, $httpParamSerializerJQLikeProvider){
		    $httpProvider.defaults.transformRequest.unshift($httpParamSerializerJQLikeProvider.$get());
		    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=utf-8';
		});


</script>