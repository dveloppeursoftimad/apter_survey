<p><?php echo $repondant->civilite ?>, </p>
<br>

<p>
	Dans le cadre du développement managérial de Civilité <span><?= $beneficiaire->ben_firstname ?></span> <span><?= $beneficiaire->ben_lastname ?></span>, nous réalisons pour <span><?= $beneficiaire->ben_societe ?></span>, une évaluation 360°Feed Back Leadership et Management.
</p>

<br>

<p>Cette démarche a pour objectif d'offrir un effet de levier positif et pertinent sur les collaborateurs qui en bénéficient. Elle est une opportunité de dresser un état des lieux autant en termes de compétences que de talents managériaux. Vous avez été choisi par Madame Sanae Benkerroum pour participer à son évaluation.</p>
<br>

<p>
	Notre déontologie ainsi que le process de collecte des données mis en place vous garantissent que vos réponses sont tout à fait confidentielles.
</p>

<p>Lien : <a href="<?php echo mvc_public_url(array('controller' => 'apter_questionnaires', 'action' => 'show', 'id' => $id));
 ?>"></a><?php echo mvc_public_url(array('controller' => 'apter_questionnaires', 'action' => 'show', 'id' => $id));
 ?></p>
 <p>Code : <b><?php echo $repondant->code ?></b></p>

<p><br>
Nous restons à votre disposition pour toute information complémentaire.

</p>
<br>


Bien cordialement  <br>
<span><?= $consultant->user_firstname ?></span> <span><?= $consultant->user_lastname ?></span> <br>
<span><?= $consultant->user_email ?></span> . <br>
Apter France <br>
