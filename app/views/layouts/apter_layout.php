
<?php get_header(); ?>


<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<article class="post-8 page type-page status-publish hentry">
				<?php $this->render_main_view(); ?>
			</article>
			
		</main><!-- #main -->
	</div><!-- #primary -->
</div>
<!-- <div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="content-wrap">
				<div class="content">
					<?php $this->render_main_view(); ?>
				</div>
								
			</div>
			
		
				
		</div>
	</div>
</div> -->

<?php get_footer(); ?>