<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
</script>
<style>
.page-numbers {
display: inline-block;
padding: 5px 10px;
margin: 0 2px 0 0;
border: 1px solid #eee;
line-height: 1;
text-decoration: none;
border-radius: 2px;
font-weight: 600;
}

.page-numbers.current,a.page-numbers:hover {
background: #d6fffb;
}
</style>
</head>
<br><br>
<h2 style="text-align: center; font-size:25px ;font-weight : bold">Liste consultants </h2>

<br><br>
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
Ajouter consultants
</button>

<!-- Modal -->
<div class="modal fade" style="margin-top:50px" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Ajouter Consultant</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		<form  name ="addConsultant" action="<?php echo mvc_admin_url(array('controller' => 'apter_ccs','action' => 'ajouterConsultant')); ?>" method="post">
			<div class="form-group">
				<label>Nom:</label>
				<input type="text" name="nom" class="form-control" aria-describedby="emailHelp" placeholder="Nom" required>
			</div>
			<div class="form-group">
				<label>Prénom:</label>
				<input type="text" name="prenom" class="form-control"  placeholder="Prénom" required>
			</div>
			<div class="form-group">
				<label for="exampleInputEmail1">Email:</label>
				<input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email" required>
  			</div>
			<div class="form-group">
				<label>Télephone:</label>
				<input type="text" name="phone" class="form-control"  aria-describedby="emailHelp" placeholder="Télephone:" required>
			</div>
			<div class="form-group">
				<label>Organisation:</label>
				<input type="text" name="organisation" class="form-control"  aria-describedby="emailHelp" placeholder="Organisation:" required>
			</div>
			<div class="form-group">
			<label>Langue:</label>
			<select class="form-control form-control-lg">
 			 	<option>Français</option>
				<option>Anglais</option>
			</select>
			</div>
			<button type="submit" class="btn btn-primary">Enregistrer</button>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>

<br>
<br>

<table class="widefat post fixed striped" cellspacing="0">
	<thead>
		<tr role='row'>
			<td style="font-weight:bold;color:#12635d">Nom & prénom</td>
			<td style="font-weight:bold;color:#12635d">Email</td>
			<td style="font-weight:bold;color:#12635d">Téléphone</td>
			<td style="font-weight:bold;color:#12635d">Organisation</td>
			<td></td>
			<td></td>
		</tr>
		
	</thead>
	<tbody>
		<?php foreach ($Apter_cc as $key => $value) { ?>

<!-- each Modal of Data -->
<div class="modal fade" style="margin-top:50px" id="exampleModalLong<?= $value->id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Ajouter Consultant</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		<form  name ="addConsultant" action="<?php echo mvc_admin_url(array('controller' => 'apter_ccs','action' => 'modifierConsultant')); ?>" method="post">
			<input id="<?= $value->id ?>" name="id" type="hidden" value="<?= $value->id ?>">
			<div class="form-group">
				<label>Nom:</label>
				<input type="text" value="<?= $value->nom ?>" name="nom" class="form-control" aria-describedby="emailHelp" placeholder="Nom" required>
			</div>
			<div class="form-group">
				<label>Prénom:</label>
				<input type="text" value="<?= $value->prenom ?>" name="prenom" class="form-control"  placeholder="Prénom" required>
			</div>
			<div class="form-group">
				<label for="exampleInputEmail1">Email:</label>
				<input type="email" value="<?= $value->email ?>" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email" required>
  			</div>
			<div class="form-group">
				<label>Télephone:</label>
				<input type="text" value="<?= $value->phone ?>" name="phone" class="form-control"  aria-describedby="emailHelp" placeholder="Télephone:" required>
			</div>
			<div class="form-group">
				<label>Organisation:</label>
				<input type="text" value="<?= $value->organisation ?>" name="organisation" class="form-control"  aria-describedby="emailHelp" placeholder="Organisation:" required>
			</div>
			<div class="form-group">
			<label>Langue:</label>
			<select class="form-control form-control-lg">
 			 	<option>Français</option>
				<option>Anglais</option>
			</select>
			</div>
			<button type="submit" class="btn btn-primary">Enregistrer</button>
		</form>

		<hr>
		<form id="modifierPassword<?= $value->id ?>" action="<?php echo mvc_admin_url(array('controller' => 'apter_ccs','action' => 'modifierPassword')); ?>" method="post">
		  <input id="<?= $value->id ?>" name="id" type="hidden" value="<?= $value->id ?>">
			<div class="form-group">
				<label>Mot de passe:</label>
				<input type="text" value="<?= $value->password ?>" name="password" class="form-control" aria-describedby="emailHelp" placeholder="Mot de passe" required>
			</div>
			<button type="submit" class="btn btn-primary">Modifer mot de passe</button>
		</form>	

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>

			<tr>
				<td><?= $value->nom ?> <?= $value->prenom ?></td>
				<td><?= $value->email ?></td>
				<td><?= $value->phone ?></td>
				<td><?= $value->organisation ?></td>
				<td><a href="#" data-toggle="modal" data-target="#exampleModalLong<?= $value->id?>">Modifier</a></td>
				<td><a href="#" onclick="confirmSuppression('<?php echo $value->nom; ?>','<?php echo $value->prenom; ?>',<?php echo $value->id; ?>)">Supprimer</a></td>
			</tr>
				
				<?php
			}

			?>
	</tbody>
</table>
<br><br>
<?php echo paginate_links($pagination); ?>

<script>
function confirmSuppression(nom,prenom, id){
		var rep=confirm("Voulez vous supprimer "+nom+" "+prenom+" ?");
		
		if(rep==true){
			location.href="<?php echo mvc_admin_url(array('controller' => 'apter_ccs', 'action' => 'delete' )); ?>&id="+id;
		}else{

	}
}
</script>
<?php /* foreach ($objects as $object): 
$this->render_view('_item', array('locals' => array('object' => $object))); 
endforeach; 
echo $this->pagination(); */ ?>
