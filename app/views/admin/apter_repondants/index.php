<head>

	<meta charset="UTF-8">
	<style>

		.ui-buttonset .ui-button {
			all:none
		}
		
		.page-numbers {
			display: inline-block;
			padding: 5px 10px;
			margin: 0 2px 0 0;
			border: 1px solid #eee;
			line-height: 1;
			text-decoration: none;
			border-radius: 2px;
			font-weight: 600;
		}

		.page-numbers.current,a.page-numbers:hover {
			background: #d6fffb;
		}
		#sparrow{
			color:white
		}
	</style>
<head>
<br><br>
<h2 style="text-align: center; font-size:25px ;font-weight : bold">Liste Coaché</h2>
<br><br>

<br><br>

<table class="widefat post fixed striped" id="myTable" cellspacing="0">
	<thead>
		<tr>
			<td>Consultant</td>
			<td>Coaché</td>
			<td>lang</td>
			<td>Supprimer</td>
		</tr>
		
	</thead>
	<tbody>
		<?php
			foreach ($Apter_repondant as $value) {
		?>
		
			<tr>	
				<td><?= $value[nom] ?> <?= $value[prenom] ?></td>
				<td><?= $value[firstname] ?> <?= $value[lastname] ?></td>
				<td><?= $value[ben_langue] ?></td>
				<td><a href="#" onclick="confirmSuppression('<?php echo $value[firstname]; ?>','<?php echo $value[lastname]; ?>',<?php echo $value[id]; ?>)">Supprimer</a></td>
			</tr>
                
		<?php
			}
		?>
		
	</tbody>
</table>
<br><br>
<?php //echo paginate_links($pagination); ?>


<script type="text/javascript">

function confirmSuppression(nom,prenom, id){
		var rep=confirm("Voulez vous supprimer "+nom+" "+prenom+" ?");
		
		if(rep==true){
			location.href="<?php echo mvc_admin_url(array('controller' => 'apter_repondants', 'action' => 'delete' )); ?>&id="+id;
		}else{

	}
}

/*

*/
$(document).ready(function() {
    $('#myTable').DataTable( {
		dom: 'Bfrtip',
		buttons: [
				{
					
					extend : "excel",
					text : 'Exporter en Excel',
					exportOptions: {
		                columns: [0,1]
		            }

				}

			]
    } );
} );
	
</script>