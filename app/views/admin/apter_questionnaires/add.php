<style type="">
.active{
		color: rgb(255,186,0) !important;
		font-weight: bold !important;
	}

	.title-head, .title-head a{
		color: grey;
		font-weight: normal; 
	}
	.center{
		text-align: center;
	}
	.hidden{
		display: none;
	}
	.question{
		font-weight: bold;
		font-size: 5;
	}
.loader {
	margin: 50px auto;
    border: 9px solid #f3f3f3; /* Light grey */
    border-top: 9px solid grey; /* Blue */
    border-radius: 50%;
    width: 60px;
    height: 60px;
    animation: spin 1s linear infinite;
}

@keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
}
</style>


<script type="text/javascript" src="<?php echo mvc_js_url("apter-survey","angular") ?>"></script>
<?php add_thickbox(); ?>


<div ng-app="apterApp" ng-controller="ApterController as apter" ng-cloak>
	<!-- <pre>
		<?php
			echo json_encode($objects);
		 ?>
	</pre> -->
	<h2>Ajouter une Questionnaire</h2>
	<h3 ng-if="apter.quest_saved" class="title-head">Résumé | <span class="active">Conception</span> |<a href="<?php echo mvc_admin_url(array('controller' => 'admin_apter_questionnaires', 'action' => 'collecte' )); ?>&id={{ apter.ApterQuestionnaire.id }}"> Collecte de réponses</a>  | Analyse</h3>
	<div >

		<div ng-if="!apter.duplicating">
			<label>Nom :</label>

		      <input ng-readonly="apter.quest_saved" type="text" ng-model="apter.ApterQuestionnaire.type" placeholder="Entrer le nom du questionnaire ici" style="width:40%">


		      <button ng-if="!apter.quest_saved" ng-click="apter.saveQuest()" >Enregistrer</button>
		      <a href="#TB_inline?width=600&height=100&inlineId=duplicate"  class="thickbox" ng-if="!apter.quest_saved">
		      	<button  >Dupliquer un questionnaire existant</button>
		      </a>
			
		</div>
	      

      <div id="duplicate"  style="display: none">
      	<br>
      		<form enctype="application/x-www-form-urlencoded" action="<?php echo mvc_admin_url(array('controller' => 'admin_apter_questionnaires', 'action' => 'duplicate' )); ?>" method="POST">
      			
      		
      		Questionnaire à dupliquer : <select name="data[id]">
      			<option  ng-repeat="quest in apter.quests" value="{{ quest.id }}" >{{ quest.type }}</option>
      			</select>
      			<br>
      			<input type="submit" class="button button-primary" ng-click="apter.duplique_load()" name="" value="Dupliquer" id="dupliquer">
      		</form>
      	
      </div>


      <a ng-if="apter.quest_saved" href="#TB_inline?width=600&height=100&inlineId=update-quest" class="thickbox"><button>Modifier</button></a>

      <div id="update-quest" style="display: none">	
      			<br>	
      			<label>	Nom du questionnaire : </label>
      			<input type="text" ng-model="apter.ApterQuestionnaire.type" placeholder="Entrer le nom du questionnaire ici" style="width:60%"><br><br>		
      			<button ng-click="apter.saveQuest()" on-click="" >Enregistrer</button>
      </div>
      <hr>
    
    </div>

    <div class="center" ng-show="apter.loading">
    	<div class="loader" ></div> 
    </div>
     

    <div ng-if="apter.quest_saved" ng-show="!apter.loading">	
    	<div class="postbox" ng-repeat="page in apter.pages">	
    		
    		<div class="inside">	
    			 <h2 class="hndle ui-sortable-handle">Questionnaire :	{{ apter.ApterQuestionnaire.type  }}</h2>
    			 <br>	
    			 	Titre : <b>{{ page.titre }}	</b><a ng-if="!page.titre" href="#TB_inline?width=150%&height=200&inlineId=add_title-{{$index}}" class="thickbox"><button ng-click="apter.init_question()">Ajouter un titre</button></a> 
    			 	<a ng-if="page.titre" href="#TB_inline?width=150%&height=200&inlineId=add_title-{{$index}}" class="thickbox"><button>	Modifier</button></a>
    			 	<br>	
    			 	<span ng-if="page.titre">Sous-titre : <b>{{ page.sous_titre }}	</b> 	</span>
    				<table class="hidden" id="add_title-{{$index}}">	
    					<tbody>	
    							<tr>	
    									<td><label>Titre de la page :</label>	</td>
    									<td><input type="text" ng-model="page.titre" placeholder="Entrer le titre de la page ici"  style="width:150%">	</td>
    							</tr>
    							<tr>	
    									<td><label>Sous-titre :</label> </td>
    									<td><input type="text" ng-model="page.sous_titre" placeholder="Entrer le sous-titre de la page ici" style="width:150%"></td>
    							</tr>
    							<tr>	
    									<td><button ng-click="apter.add_title(page)">Enregistrer</button></td>
    									<td>	</td>
    							</tr>
    					</tbody>
    				</table>
    			

    			<hr>	

    			<div ng-repeat="question in page.questions">	
    					<span ng-show="question.question"><span class="question">Q {{ $index +1 }} - {{ question.question }}</span> <a href="#TB_inline?width=150%&height=300&inlineId=edit_question-{{$parent.$index}}-{{$index}}" class="thickbox" ng-click="apter.init_question_edit(question)"><button style="font-weight: normal;" ng-show="question.id">Modifier</button></a></span>
    					<ul ng-show="question.question">
    						<li ng-show="question.type =='radio'" ng-repeat="reponse in question.reponses" >- {{ reponse.reponse }}</li>
    					</ul>

    					<div id="edit_question-{{$parent.$index}}-{{$index}}" class="hidden">
	    				<form name="questionForm" novalidate>
	    					<span style='color: red'>	{{ apter.questionFormError }}</span>	
	    					<table class="">	
			    					<tr>	
			    							<td>Question : </td>
			    							<td><textarea  rows="2" cols="60" ng-model="question.question"></textarea>

			    							
			    						</td>
			    					</tr>
			    					<tr>	
			    							<td>Type : </td>
			    							<td><select ng-model="question.type">	
			    									<option value="radio">Choix de réponse</option>
			    									<option value="text">Libre</option>
			    							</select>
			    						</td>
			    					</tr>
			    					
			    			</table>
			    				
			    			
			    			<div ng-show="question.type == 'radio'">	
			    				<label><input type="checkbox" ng-model="question.negative" ng-true-value="1" ng-false-value="0"> Question négative ?</label>
			    				
			    				<hr>
			    				<div  ng-repeat="reponse in question.reponses">	
			    					

			    						<span> Réponse {{$index +1 }} : <input type="text
			    							" ng-model="reponse.reponse" required>{{ apter.reponseForm[$index].reponse.$valid }}</span>
			    						<span> Valeur : 
			    							<select ng-model="reponse.score">	
			    								<option value="0">0</option>
			    								<option value="1">1</option>
			    								<option value="2">2</option>
			    								<option value="3">3</option>
			    								<option value="4">4</option>
			    						</select></span>
			    						<span>	<button ng-click="apter.remove_reponse_edit(question, $index)">	Supprimer</button></span>
			    					
			    						
			    			

				    			</div>
				    			<button ng-click="apter.add_reponse_edit(question)">+</button>
				    			<hr>	

			    			</div>
			    			
			    			<br>	
			    			<button ng-click="apter.edit_question(page, question)">Enregistrer</button>

	    				</form>
	    				

	   				</div>

    			

	    			
	    			

	   			</div>

	   			<div id="add_question-{{$index}}" class="hidden">
	    				<form name="questionForm" novalidate>
	    					<span style='color: red'>	{{ apter.questionFormError }}</span>	
	    					<table class="">	
			    					<tr>	
			    							<td>Question : </td>
			    							<td><textarea  rows="2" cols="60" ng-model="apter.question"></textarea>

			    							
			    						</td>
			    					</tr>
			    					<tr>	
			    							<td>Type : </td>
			    							<td><select ng-model="apter.type">	
			    									<option value="radio">Choix de réponse</option>
			    									<option value="text">Libre</option>
			    							</select>
			    						</td>
			    					</tr>
			    					
			    			</table>
			    				
			    			
			    			<div ng-show="apter.type == 'radio'">	
			    				<label><input type="checkbox" ng-model="apter.negative" ng-true-value="1" ng-false-value="0"> Question négative ?</label>
			    				<hr>
			    				<div  ng-repeat="reponse in apter.reponses">	
			    					

			    						<span> Réponse {{$index +1 }} : <input type="text
			    							" ng-model="reponse.reponse" required>{{ apter.reponseForm[$index].reponse.$valid }}</span>
			    						<span> Valeur : 
			    							<select ng-model="reponse.score">	
			    								<option value="0">0</option>
			    								<option value="1">1</option>
			    								<option value="2">2</option>
			    								<option value="3">3</option>
			    								<option value="4">4</option>
			    						</select></span>
			    						<span>	<button ng-click="apter.remove_reponse($index)">	Supprimer</button></span>
			    					
			    						
			    			

				    			</div>
				    			<button ng-click="apter.add_reponse()">+</button>
				    			<hr>	

			    			</div>
			    			
			    			<br>	
			    			{{ angular.isDefined(apter.reponses[0].reponse) }}
			    			<button ng-click="apter.add_question(page)">Enregistrer</button>

	    				</form>
	    				

	   				</div>	

	   			<div class="center"><a href="#TB_inline?width=150%&height=300&inlineId=add_question-{{$index}}" class="thickbox" ng-click="apter.init_question()"><button  >Ajouter une question</button></a></div>

    			

	    		
	    		
    		</div>
    		
    	</div>
    	
    	<div class="center">
    		<button ng-show="apter.pages[apter.pages.length - 1]['questions'][0] != undefined" ng-click="apter.add_page()">Nouvelle page</button>
    	</div>
    	
    		


    </div>	

    <!-- MODALS -->
    



	

</div>



<!-- <pre>
	<?php var_dump($model) ?>
</pre> -->
<!-- <?php echo $this->form->create($model->name); ?>
<?php echo $this->form->input('type',['label' => "Nom du questionnaire"]); ?>
<?php echo $this->form->end('Enregistrer'); ?> -->

    



<script type="text/javascript">
	var app = angular.module('apterApp',[])
		.controller('ApterController', function($http){
			var apter = this;
			apter.quest = {
				nom : "Test Apter",
				id: undefined
			};
			apter.quest_saved = false;
			apter.loading = false;
			apter.duplicating = false;
			apter.pages = [{
				title_added: false,
				questions: []
			}];

			<?php
				if(isset($objects)){
					?>
					apter.quests = <?php echo json_encode($objects); ?>;

					<?php
				}
			?>

			apter.question = "";
			apter.type = "";
			apter.negative = 0;
			apter.reponses = [{
				reponse : "pas du tout d'accord",
				score: "1"
			},{
				reponse : "plutot pas d'accord",
				score: "2"
			},{
				reponse : "plutot d'accord",
				score: "3"
			},{
				reponse : "tout à fait d'accord",
				score: "4"
			},{
				reponse : "je ne sais pas",
				score: "0"
			}];
			
			apter.ApterPage = {};

			apter.duplique_load = function(){
				tb_remove();
				apter.loading = true;
				apter.duplicating = true;
			}

			apter.add_title = function(page){
				tb_remove();
				apter.ApterPage = page;
				console.log(apter);
				var data = {
					ApterPage: page,
					ApterQuestionnaire:{
						id: apter.ApterQuestionnaire.id
					}
				}
				if(page.titre != ""){
					$http({
					  method: 'POST',
					  url: ajaxurl,
					  data: {
					  	action : "admin_apter_questionnaires_ajax_page_title",
					  	data : data

					  }
					}).then(function successCallback(response) {
					    page.id = response.data;
					    console.log(page);
					  }, function errorCallback(response) {
					    // called asynchronously if an error occurs
					    // or server returns response with an error status.
					  });
				}
				console.log(page);
			}

			apter.init_question = function(){
				apter.question = "";
				apter.type = "";
				apter.negative = 0;
				apter.reponses = [{
				reponse : "pas du tout d'accord",
					score: "1"
				},{
					reponse : "plutot pas d'accord",
					score: "2"
				},{
					reponse : "plutot d'accord",
					score: "3"
				},{
					reponse : "tout à fait d'accord",
					score: "4"
				},{
					reponse : "je ne sais pas",
					score: "0"
				}];
				console.log(apter.reponses);
				apter.questionFormError = "";
			}

			apter.init_question_edit = function(question){
				apter.question = question.question;
				apter.type = question.type;
				apter.reponses = question.reponses;
				apter.questionFormError = "";
			}

			apter.add_question = function(page){
				


				if(apter.type == "radio" ? (apter.question != "" && apter.reponses[0].reponse != undefined) : (apter.question != "" && apter.type != "")){
					apter.loading = true;
					tb_remove();
					if(apter.type == "radio"){
						apter.reponses = apter.reponses.filter(elt => angular.isDefined(elt.reponse) );
					}
					
					var question = {
						question : apter.question,
						type: apter.type,
						reponses: apter.reponses,
						negative : apter.negative
					}
					

					if(page.id){
						question.apter_page_id = page.id;
					}
					if(apter.ApterQuestionnaire.id){
						question.apter_questionnaire_id = apter.ApterQuestionnaire.id;
					}
					apter.ApterQuestion = question;
					page.apter_questionnaire_id = apter.ApterQuestionnaire.id;
					apter.ApterPage = page;
					var data = {
						ApterQuestion : apter.ApterQuestion,
						ApterPage : apter.ApterPage
					}
					$http({
					  method: 'POST',
					  url: ajaxurl,
					  data: {
					  	action : "admin_apter_questionnaires_ajax_add_question",
					  	data : data

					  }
					}).then(function successCallback(response) {
						apter.loading = false;
					    question.id = response.data.id;
					    page.id = response.data.apter_page_id;
					   if(!angular.isDefined(page.questions)){
					    	page.questions = [];
					    }
					    page.questions.push(question);
					    console.log(page.questions);
					    console.log(page.questions.length);
					    console.log(response);
					  }, function errorCallback(response) {
					    // called asynchronously if an error occurs
					    // or server returns response with an error status.
					  });
					
					
				}else{
					apter.questionFormError = "Veuillez bien remplir les champs";
				}
			}

			apter.edit_question = function(page, question){
				console.log(question);
				if(question.type == "radio" ? (question.question != "" && question.reponses[0].reponse != undefined) : (question.question != "" && question.type != "")){
					tb_remove();
					question.reponses = question.reponses.filter(elt => angular.isDefined(elt.reponse)
						);
					
					

					if(page.id){
						question.apter_page_id = page.id;
					}
					if(apter.ApterQuestionnaire.id){
						question.apter_questionnaire_id = apter.ApterQuestionnaire.id;
					}
					apter.ApterQuestion = question;
					page.apter_questionnaire_id = apter.ApterQuestionnaire.id;
					apter.ApterPage = page;
					var data = {
						ApterQuestion : apter.ApterQuestion,
						ApterPage : apter.ApterPage
					}
					$http({
					  method: 'POST',
					  url: ajaxurl,
					  data: {
					  	action : "admin_apter_questionnaires_ajax_add_question",
					  	data : data

					  }
					}).then(function successCallback(response) {
					    question.id = response.data.id;
					    page.id = response.data.apter_page_id;
					    console.log(response);
					  }, function errorCallback(response) {
					    // called asynchronously if an error occurs
					    // or server returns response with an error status.
					  });
					
					
				}else{
					apter.questionFormError = "Veuillez bien remplir les champs";
				}
			}

			apter.add_reponse = function(){
					apter.reponses.push({});
			}
			apter.reponse_valid = function(){
				for(var i in apter.reponses){

				}
			}

			apter.remove_reponse = function(index){
				apter.reponses.splice(index,1);
			}

			apter.remove_reponse_edit = function(question, index){
				question.reponses.splice(index,1);
			}

			apter.add_reponse_edit = function(question){
				question.reponses.push({});
			}

			apter.saveQuest = function(){
				tb_remove();
				
				if(apter.ApterQuestionnaire.type && apter.ApterQuestionnaire.type != ""){
					apter.loading = true;
					var data = {
						type: apter.ApterQuestionnaire.type
					}
					if(angular.isDefined(apter.ApterQuestionnaire.id)){
						data.id = apter.ApterQuestionnaire.id;
					}
					$http({
					  method: 'POST',
					  url: ajaxurl,
					  data: {
					  	action : "admin_apter_questionnaires_ajax_add",
					  	data : data

					  }
					}).then(function successCallback(response) {
						apter.loading = false;
					    apter.quest_saved = true
					    console.log(response);
					    if(angular.isUndefined(apter.ApterQuestionnaire.id)) apter.ApterQuestionnaire.id = parseInt(response.data);
					  }, function errorCallback(response) {
					    // called asynchronously if an error occurs
					    // or server returns response with an error status.
					  });
				}
					
			}

			apter.add_page = function(){
			

				apter.pages.push({
				});

				console.log(apter.pages);
			}
			
		});
	app.config(function ($httpProvider, $httpParamSerializerJQLikeProvider){
	    $httpProvider.defaults.transformRequest.unshift($httpParamSerializerJQLikeProvider.$get());
	    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=utf-8';
	});
</script>