
<h2>Tous les sondages</h2>
<div>
	<a href="<?php echo mvc_admin_url(array('controller' => 'admin_apter_questionnaires', 'action' => 'add' )); ?>"><button class="button button-primary">Ajouter</button></a>
	
</div>

<table class="widefat post fixed striped" cellspacing="0">
	<thead>
		<tr>
			<td>Titre</td>
			<td>Réponses</td>
			<td>Conception</td>
			<td>Collecter</td>
			<td>Analyse</td>
			<td></td>
			<td></td>
		</tr>
		
	</thead>
	<tbody>
		<?php
			foreach ($objects as $key => $value) {
				?>
				<tr>
					<td><a href="#"><?= $value->type ?> </a><br> <small><?= $value->date ?></small></td>
					<td><?= $value->nb_resultat ?></td>
					<td><a href="<?php echo mvc_admin_url(array('controller' => 'admin_apter_questionnaires', 'action' => 'edit', 'id' => $value->id )); ?>"><span class="dashicons dashicons-welcome-write-blog"></span></a></td>
					<td><a href="<?php echo mvc_admin_url(array('controller' => 'admin_apter_questionnaires', 'action' => 'collecte', 'id' => $value->id )); ?>"><span class="dashicons dashicons-share-alt2"></span></a></td>
					<td><a href="<?php echo mvc_admin_url(array('controller' => 'admin_apter_questionnaires', 'action' => 'analyse', 'id' => $value->id )); ?>"><span class="dashicons dashicons-chart-bar"></span></a></td>
					<td><a onclick="confirmSuppression('<?php echo $value->type; ?>', <?php echo $value->id; ?>)"><span class="dashicons dashicons-trash"></span></a>
					</td>
					<td><a href="<?php echo mvc_admin_url(array('controller' => 'admin_apter_questionnaires', 'action' => 'reponses', 'id' => $value->id )); ?>">Tableau des réponses </a></td>
				</tr>

				<?php
			}

		?>
		
	</tbody>
</table>

<div class="tablenav">

    <div class="tablenav-pages">
    
        <?php echo paginate_links($pagination); ?>
    
    </div>

</div>

<div>
	<a href="<?php echo mvc_admin_url(array('controller' => 'admin_apter_questionnaires', 'action' => 'add' )); ?>"><button class="button button-primary">Ajouter</button></a>
	
</div>

<script type="text/javascript">
	function confirmSuppression(quest, id){
		var rep=confirm("Voulez vous supprimer le questionnaire "+quest+" ?");
		
		if(rep==true){
			location.href="<?php echo mvc_admin_url(array('controller' => 'admin_apter_questionnaires', 'action' => 'supprimer' )); ?>&id="+id;
		}else{

		}
	}
</script>