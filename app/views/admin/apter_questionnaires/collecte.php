<script type="text/javascript" src="<?php echo mvc_js_url("apter-survey","angular") ?>"></script>
<style type="text/css">
	.active{
		color: rgb(255,186,0) !important;
		font-weight: bold !important;
	}

	.title-head, .title-head a{
		color: grey;
		font-weight: normal; 
	}
	.right{
		text-align: right;
	}
	.error{
		color: red;
	}
	.loader {
	margin: 50px auto;
    border: 9px solid #f3f3f3; /* Light grey */
    border-top: 9px solid grey; /* Blue */
    border-radius: 50%;
    width: 60px;
    height: 60px;
    animation: spin 1s linear infinite;
}
<?php for ($i=0; $i < 10; $i++) { 
	?>
	.col-<?php echo $i; ?>{
		width: <?php echo $i; ?>0%;
		display: inline-block;
	}

	<?php
} ?>


@keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
}
</style>

<?php add_thickbox(); ?>

<div ng-app="apterApp" ng-controller="ApterController as apter" ng-cloak>

	<div class="center" ng-show="apter.loading">
    	<div class="loader" ></div> 
    </div>
    <div ng-show="!apter.loading">
    	<h2>{{ apter.ApterQuestionnaire.type }}</h2>
		<h3 class="title-head">Résumé | <a href="<?php echo mvc_admin_url(array('controller' => 'admin_apter_questionnaires', 'action' => 'edit', 'id' => $id )); ?>">Conception</a> | <span class="active">Collecte de réponses</span> | <a href="<?php echo mvc_admin_url(array('controller' => 'admin_apter_questionnaires', 'action' => 'analyse', 'id' => $id )); ?>">Analyse</a></h3>
		
		


			

		<div ng-show="apter.beneficiaire" >
			<p class="error">{{ apter.data_valid_message }}</p>
			<h3>Bénéficiaire trouvé :</h3>
			<table class="widefat post fixed striped" cellspacing="0">
				<thead>
					<tr>
						<td>Civilité</td>
						<td>Prénom</td>
						<td>Nom</td>
						<td>Société</td>
						<td>Initiales</td>
						<td>Email</td>
						<td>Tel. portable</td>
					</tr>
					
				</thead>
				<tbody>
					<tr>
						<td>{{ apter.beneficiaire.ben_civilite }}</td>
						<td>{{ apter.beneficiaire.ben_firstname }}</td>
						<td>{{ apter.beneficiaire.ben_lastname }}</td>
						<td>{{ apter.beneficiaire.ben_societe }}</td>
						<td>{{ apter.beneficiaire.ben_initiales }}</td>
						<td>{{ apter.beneficiaire.ben_email }} <span class="error">{{ apter.beneficiaire.ben_email_error }}</span></td>
						<td>{{ apter.beneficiaire.ben_tel }}</td>
						
					</tr>
				</tbody>
			</table>

			<h3>Répondants trouvés :</h3>
				<table class="widefat post fixed striped" cellspacing="0">
					<thead>
						<tr>
							<td>Nom du groupe * </td>
							<td>Abréviation * </td>
							<td>Civilité</td>
							<td>Prénom *</td>
							<td>Nom *</td>
							<td>Email *</td>
						</tr>
						
					</thead>
					<tbody>
						<tr ng-repeat="repondant in apter.repondants">
							<td>{{ repondant.groupe }}</td>
							<td>{{ repondant.abrev }} <span class="error">  {{ repondant.abrev_error }}</span></td>
							<td>{{ repondant.civilite }}</td>
							<td>{{ repondant.firstname }}<span class="error">  {{ repondant.firstname_error }}</span></td>
							<td>{{ repondant.lastname }}<span class="error">  {{ repondant.lastname_error }}</span></td>
							<td>{{ repondant.email }}<span class="error">  {{ repondant.email_error }}</span></td>
						</tr>
					</tbody>
						
				</table>
			<div class="">
				<br>
				<button ng-click="apter.validate()" class="button button-primary" ng-show="apter.data_valid">Valider</button>
				<button disabled="true" ng-show="!apter.data_valid">Valider</button>
			</div>

		</div>

		<div ng-show="apter.ApterRepondants.length >0 && !apter.data_import">
			<div class="postbox">
				<div class="inside">
					<h3>Bénéficiaire</h3>
					<table class="widefat post fixed striped" cellspacing="0">
						<thead>
							<tr>
								<td>Civilité</td>
								<td>Prénom</td>
								<td>Nom</td>
								<td>Société</td>
								<td>Initiales</td>
								<td>Email</td>
								<td>Tel. portable</td>
								<td>Code</td>
							</tr>
							
						</thead>
						<tbody>
							<tr>
								<td>{{ apter.ApterQuestionnaire.ben_civilite }}</td>
								<td>{{ apter.ApterQuestionnaire.ben_firstname }}</td>
								<td>{{ apter.ApterQuestionnaire.ben_lastname }}</td>
								<td>{{ apter.ApterQuestionnaire.ben_societe }}</td>
								<td>{{ apter.ApterQuestionnaire.ben_initiales }}</td>
								<td>{{ apter.ApterQuestionnaire.ben_email }}</td>
								<td>{{ apter.ApterQuestionnaire.ben_tel }}</td>
								<td>{{ apter.ApterQuestionnaire.ben_code }}</td>
								
							</tr>
						</tbody>
					</table>
				</div>
				
			</div>
			<div>
				<button class="button button-primary" ng-click="apter.send_mail()"><span class="dashicons dashicons-share-alt2"> </span> Envoyer les mails aux répondants</button>
			</div>
			<div class="postbox">
				<div class="inside">
					<h3>Répondants</h3>
					<p><i><b>Lien de connexion :</b></i> <a href="<?php echo mvc_public_url(array('controller' => 'apter_questionnaires', 'action' => 'show')) ?>{{ apter.ApterQuestionnaire.id }}"><?php echo mvc_public_url(array('controller' => 'apter_questionnaires', 'action' => 'show')) ?>{{ apter.ApterQuestionnaire.id }}</a></p>
					<div style="text-align: center;" ng-show="apter.repondantSelected">
						<!-- <label>Actions</label>
						<select ng-model="apter.repondantsAction" ng-change="apter.repondantsActionChange()">
							
							<option value="relance">Relancer</option>
						</select> -->
						<button class="default" ng-click="apter.repondantsActionChange()"> <span class="dashicons dashicons-share-alt2"> </span> Relancer les répondants selectionnés</button>
					</div>
					<table class="widefat post fixed striped" cellspacing="0" id="repondant-table">
						<thead>
							<tr>
								<td></td>
								<td>Nom du groupe  </td>
								<td>Abréviation du groupe  </td>
								<td>Civilité</td>
								<td>Prénom </td>
								<td>Nom </td>
								<td>Email </td>
								<td>Code </td>
								<td>Envoi mail </td>
								<td>Réponse </td>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="rep in apter.ApterRepondants">
								<td><input ng-show="rep.send != 0 && !rep.reponse" type="checkbox" ng-model="rep.selected" ng-checked="apter.selectRepondant()"></td>
								<td>{{ rep.apter_groupe.nom }}</td>
								<td>{{ rep.apter_groupe.abreviation }} </td>
								<td>{{ rep.civilite }}</td>
								<td>{{ rep.firstname }}</td>
								<td>{{ rep.lastname }}</td>
								<td>{{ rep.email }}</td>
								<td>{{ rep.code }}</td>
								<td ng-show="rep.send == 0 ">
									<button class="button button-default" ng-click="apter.send_mail_repondant(rep.id)">Envoyer un mail</button>

								</td>
								<td>
									<button ng-show="rep.send != 0 && !rep.reponse " class="button button-default" ng-click="apter.send_relance_mail_repondant(rep.id)">Relancer</button>
								</td>
								<td>{{ rep.reponse ? "Oui" : "Non" }}</td>
							</tr>
							
						</tbody>
						
						
					</table>

					<div>	
							<a href="#TB_inline?width=150%&height=400&inlineId=add_repondant" class="thickbox"><button class="button button-primary">Ajouter un Répondant</button></a>
					</div>


				</div>


				
			</div>
			
		</div>

		<div id="add_repondant" class="hidden">
	    				<form name="apter.repondantForm" novalidate>
	    					<h3>Ajouter un répondant</h3>
	    					<span style='color: red'>	{{ apter.repondantFormError }}</span>	
	    					<table>	
	    						<tr>	
	    							<td>Civilité : </td>
	    							<td><input type="text" ng-model="apter.NewRepondant.civilite" required></td>

	    						</tr>
	    						<tr>	
	    							<td>Nom : </td>
	    							<td><input type="text" ng-model="apter.NewRepondant.lastname" required></td>
	    						</tr>
	    						<br>
	    						<tr>	
	    							<td>Prénom(s) : </td>
	    							<td><input type="text" ng-model="apter.NewRepondant.firstname" required></td>
	    						</tr>
	    						<br>
	    						<tr>	
	    							<td>Email : </td>
	    							<td><input type="email" ng-model="apter.NewRepondant.email" required></td>
	    						</tr>

	    						<tr>	
	    									<td>Groupe</td>
	    									<td>	<input type="text" ng-model="apter.NewRepondant.groupe" required></td>
	    						</tr>
	    							<tr>	
	    									<td>Abréviation du groupe</td>
	    									<td>	<input type="text" ng-model="apter.NewRepondant.abrev" required></td>
	    							</tr>
	    					</table>
	    					<hr>	
	    					<button  class="button button-primary" ng-click="apter.add_repondant()">Ajouter</button>
	    				</form>
	    </div>

		<br ng-show="apter.beneficiaire">
		<div class="postbox" >
			<div class="inside">
				<form enctype="multipart/form-data" action="<?php echo mvc_admin_url(array('controller' => 'admin_apter_questionnaires', 'action' => 'collecte', 'id' => $id)); ?>" method="post">	

					Fichier Excel :
					<input type="file" name="apter-file">
					<?php submit_button('Importer') ?>

				</form>
				
			</div>
		</div>
    	
    </div>
		

	
			

</div>


<script type="text/javascript">
	var app = angular.module('apterApp',[])
		.controller('ApterController', function($http){
			var apter = this;
			apter.loading = false;
			apter.data_import = false;
			apter.repondantSelected =  false;
			apter.ApterRepondants = [];
			apter.NewRepondant = {
				'civilite': "",
				'lastname': "",
				'firstname': "",
				'email': "",
				'abrev': "",
				'groupe': ""
			}
			<?php 
				if(isset($id)){
					?>
					apter.id = <?php echo intval($id); ?>;
					<?php
				}
			?>
			<?php 
				if(isset($object)){
					?>
					apter.ApterQuestionnaire = <?php echo json_encode($object); ?>;
					<?php
				}
			?>
			<?php 
				if(isset($beneficiaire)){
					?>
					apter.beneficiaire = <?php echo json_encode($beneficiaire); ?>;

					<?php
				}
			?>
			<?php 
				if(isset($ApterBeneficiaire)){
					?>
					apter.ApterBeneficiaire = <?php echo json_encode($ApterBeneficiaire); ?>;

					<?php
				}
			?>
			<?php 
				if(isset($repondants)){
					?>
					apter.repondants = <?php echo json_encode($repondants); ?>;
					<?php
				}
			?>
			<?php 
				if(isset($ApterRepondants)){
					?>
					apter.ApterRepondants = <?php echo json_encode($ApterRepondants); ?>;
					console.log(apter.ApterRepondants);
/*
					for(rep in apter.ApterRepondants){
						rep.selected = false;
					}*/

					<?php
				}
			?>
			<?php 
				if(isset($data_valid)){
					?>
					apter.data_valid = <?php echo $data_valid ? "true" : "false"; ?>;
					<?php
				}
			?>
			<?php 
				if(isset($data_import)){
					?>
					apter.data_import = <?php echo $data_import ? "true" : "false"; ?>;
					<?php
				}
			?>
			if(angular.isDefined(apter.beneficiaire) && !apter.data_valid){
				apter.data_valid_message = "* Certains champs du fichier Excel importé ne sont pas valides. \n Veuillez corriger et importez à nouveau";
			}

			apter.selectRepondant = function(){
				for(i in apter.ApterRepondants){
					if(apter.ApterRepondants[i].selected){
						apter.repondantSelected =  true;
						break;
					}else{
						apter.repondantSelected =  false;
					}
				}
				
			}

			apter.repondantsActionChange = function(){
				
					repondants_relance = [];
					for(i in apter.ApterRepondants){
						if(apter.ApterRepondants[i].selected){
							repondants_relance.push(apter.ApterRepondants[i].id)

						}
					}
					apter.send_relance_mail_repondants(repondants_relance);


			}
			
			apter.add_repondant = function(){
				apter.repondantFormError = "";
				console.log(apter.repondantForm.$valid);
			
				if(apter.repondantForm.$valid ){
					tb_remove();
					apter.loading = true
					console.log("Adding repondant");
					console.log(apter.NewRepondant);
					var data ={
						id: apter.id,
						ApterRepondant: apter.NewRepondant
					}
					$http({
					  method: 'POST',
					  url: ajaxurl,
					  data: {
					  	action : "admin_apter_questionnaires_ajax_add_repondant",
					  	data : data

					  }
					}).then(function successCallback(response) {
							apter.loading = false;
							delete apter.beneficiaire;
							delete apter.repondants;
							apter.data_import = false;
						    console.log(response);
						    apter.ApterQuestionnaire = response.data.beneficiaire;
						    apter.ApterRepondants = response.data.repondants;
						  }, function errorCallback(response) {
						    // called asynchronously if an error occurs
						    // or server returns response with an error status.
					});
					
				}else{
					apter.repondantFormError = "Veuillez bien remplir le formulaire";
				}
				
			}

			apter.validate= function(){
				apter.loading = true
				console.log(apter.beneficiaire);
				var data ={
					id: apter.id,
					repondants: apter.repondants,
					beneficiaire: apter.beneficiaire
				}
				$http({
					  method: 'POST',
					  url: ajaxurl,
					  data: {
					  	action : "admin_apter_questionnaires_ajax_add_repondants",
					  	data : data

					  }
					}).then(function successCallback(response) {
						apter.loading = false;
						delete apter.beneficiaire;
						delete apter.repondants;
						apter.data_import = false;
					    console.log(response);
					    apter.ApterQuestionnaire = response.data.beneficiaire;
					    apter.ApterRepondants = response.data.repondants;
					  }, function errorCallback(response) {
					    // called asynchronously if an error occurs
					    // or server returns response with an error status.
				});
			}

			apter.send_mail = function(){				
				
				window.location.replace("<?php echo mvc_admin_url(array('controller' => 'admin_apter_questionnaires', 'action' => 'send_mail', 'id' => $id )); ?>");


			}

			apter.send_mail_repondant = function(repondant_id){				
				
				window.location.replace("<?php echo mvc_admin_url(array('controller' => 'admin_apter_questionnaires', 'action' => 'send_mail_repondant', 'id' => $id )); ?>"+"&repondant_id="+repondant_id);


			}

			apter.send_relance_mail_repondant = function(repondant_id){				
				
				window.location.replace("<?php echo mvc_admin_url(array('controller' => 'admin_apter_questionnaires', 'action' => 'send_relance_mail_repondant', 'id' => $id )); ?>"+"&repondant_id="+repondant_id);


			}

			apter.send_relance_mail_repondants = function(repondants_id){				
				
				window.location.replace("<?php echo mvc_admin_url(array('controller' => 'admin_apter_questionnaires', 'action' => 'send_relance_mail_repondants', 'id' => $id )); ?>"+"&repondants="+repondants_id);


			}


		});
	app.config(function ($httpProvider, $httpParamSerializerJQLikeProvider){
	    $httpProvider.defaults.transformRequest.unshift($httpParamSerializerJQLikeProvider.$get());
	    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=utf-8';
	});
</script>