
<h2><?php echo $objects->type ?> | Réponses</h2>
<table class="table table-striped" id="myTable">
	<thead>
		<tr>
			<th>Répondants</th>
			<th>Groupe</th>
			<?php 

			foreach ($objects->apter_pages as $key => $value) {
				foreach ($value->apter_questions as $key2 => $question) {
					?>
					<th><?php 
					$p = $key +1;
					$q = $key2 +1;
					echo "P.".$p." Q.".$q;
					 ?></th>
					<?php
				}
			}

			 ?>

		</tr>
	</thead>
	<tbody>
		<?php foreach ($repondants as $key => $value) {
				?>
				<tr>
					<?php $reponse = json_decode($value->reponse) ?>
					<td><?php echo $value->firstname." ".$value->lastname ?></td>
					<td><?php echo $reponse[0]->groupe ;
					?></td>
					<?php 
					if(isset($reponse)){
						foreach ($reponse as $key2 => $value2) {
							?>
							<td><?php echo $value2->score ?></td>
							<?php
						}
					}else{
						foreach ($objects->apter_pages as $key => $value) {
							foreach ($value->apter_questions as $key2 => $question) {
								?>
								<td></td>
								<?php
							}
						}
					}
					
					 ?>
				</tr>
				
				<?php
			} ?>
		
	</tbody>
</table>
	
<script type="text/javascript">
	$(document).ready(function(){
		
	    $('#myTable').DataTable({
	    	dom: 'Bfrtip',
			buttons: [
				{
					extend : "excel",
					text : 'Exporter en Excel',
					

				}

			]
		});
	});
</script>
