
<style type="">
	.center{
		text-align: center;
	}
	.hidden{
		display: none;
	}
	.error{
		color: red;
	}
	.question{
		font-weight: bold;
		font-size: 5;
	}
	.active{
		color: rgb(255,186,0) !important;
		font-weight: bold !important;
	}

	.title-head, .title-head a{
		color: grey;
		font-weight: normal; 
	}
.loader {
	margin: 50px auto;
    border: 9px solid #f3f3f3; /* Light grey */
    border-top: 9px solid grey; /* Blue */
    border-radius: 50%;
    width: 60px;
    height: 60px;
    animation: spin 1s linear infinite;
}

@keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
}
</style>

<script type="text/javascript" src="<?php echo mvc_js_url("apter-survey","angular") ?>"></script>

<script type="text/javascript" src="<?php echo mvc_js_url("apter-survey","Chart") ?>"></script>
<script type="text/javascript" src="<?php echo mvc_js_url("apter-survey","jquery") ?>"></script>
<script type="text/javascript" src="<?php echo mvc_js_url("apter-survey","html2canvas") ?>"></script>


<!-- <pre>
	<?php
		echo count($repondants);
	?>
</pre> -->

<div ng-app="apterApp" ng-controller="ApterController as apter" ng-cloak>
	<h2>{{ apter.ApterQuestionnaire.type }}</h2>
		<h3  id="nav-head" class="title-head">Résumé | <a href="<?php echo mvc_admin_url(array('controller' => 'admin_apter_questionnaires', 'action' => 'edit', 'id' => $id )); ?>">Conception</a> | <a href="<?php echo mvc_admin_url(array('controller' => 'admin_apter_questionnaires', 'action' => 'collecte', 'id' => $id )); ?>">Collecte de réponses</a></span> |  <span class="active">Analyse</span></h3>

		<div class="center" ng-show="apter.loading">
	    	<div class="loader" ></div> 
	    </div>
		<div class="postbox" style="width: 100%;" ng-show="!apter.loading">
			<div class="inside">
				<p>
					<a href="#" class="{{ apter.isSet(1) ? 'active' : '' }}" ng-click="apter.setTab(1)">
						Représentation Radar
					</a>
					 >
					 <a href="#" class="{{ apter.isSet(2) ? 'active' : '' }}" ng-click="apter.setTab(2)">Dimensions principales</a>
					 >
					<a href="#" class="{{ apter.isSet(3) ? 'active' : '' }}" ng-click="apter.setTab(3)">Dimensions détaillées</a> >
					<a href="#" class="{{ apter.isSet(4) ? 'active' : '' }}" ng-click="apter.setTab(4)">Moyenne par question</a> >

					<a href="#" class="{{ apter.isSet(5) ? 'active' : '' }}" ng-click="apter.setTab(5)">Questions ouvertes</a> >

					<span>
						<button id="save" ng-click="apter.export()" >Générer un rapport docx</button> 
						<span ng-show="apter.rapport"> > <a href="<?php echo mvc_plugin_app_url('apter-survey').'public/uploads/'.$id.'/Rapport_'.$objects->type.'.docx';?>" >  <button >Télécharger le rapport</button></a></span>
						
						
					</span>
				</p>

				<div>
					<p class="error">{{ apter.generete_error }}</p>
					<i>
						Nombre de répondants : <b>{{ apter.nb_repondants }} </b><br>
					Nombre de réponse : <b><?php echo $nb_reponse ?></b>
					</i>
					
				</div>
			</div>
			
		</div>
		
		<div style="width: 80%;" ng-show="apter.isSet(4)" class="postbox">
			<!-- <div ng-repeat="page in apter.ApterQuestionnaire.apter_pages">
				<div ng-repeat="question in page.apter_questions">
					<h4>{{ question.question }}</h4>
					<canvas id="question-chart-{{$parent.$index}}-{{$index}}" width="50" height="50"></canvas>
				</div>

					
				
			</div> -->
			<div id="chart_questions" class="inside">
				
			</div>

			<canvas id="myChart" width="50" height="50"></canvas>
		</div>
		<div style="width: 80%;"  ng-show="apter.isSet(3)" class="postbox">
			<div id="chart_details" class="inside">
				
			</div>
			
		</div>

		<div style="width: 80%;"  ng-show="apter.isSet(2)" class="postbox">
			<div id="chart_principales" class="inside">
				
			</div>
			
		</div>


		<div style="width: 60%;"  ng-show="apter.isSet(1)" class="postbox">
			<div id="chart_radar" class="inside">
				
			</div>
			
		</div>

		<div style="width: 100%;"  ng-show="apter.isSet(5)" class="postbox">
			
			<div class="inside">


				<div id="question_ouverte_image">
				
				</div>
			</div>
			
			
		</div>

		<div style="opacity: 0.0">
			<div id="question_ouverte">
				
			</div>
		</div>

		
		
		

</div>

<script>


</script>

<script type="text/javascript">
	var app = angular.module('apterApp',[])
		.controller('ApterController', function($http, $timeout){
			var apter = this;
			//init data
			apter.ApterQuestionnaire = <?php echo json_encode($objects)?>;
			apter.nb_repondants = <?php echo count($repondants) ?>;
			apter.data = [];
			apter.data_per_question = [];
			apter.data_per_question_ouverte = [];
			apter.questions = [];
			apter.data_detail = [];
			apter.data_principal = [];
			apter.data_radar = [];
			apter.tab = 1;
			apter.loading = false;
			apter.rapport = false;
			apter.generete_error = "";


			apter.load_detail = function(){
				apter.tab = 3;
			}

			apter.export = function(){
				apter.generete_error = "";
				/*for (var i = 1; i < 5; i++) {
					apter.tab = i;
				}*/
				apter.tab = 5;
				apter.loading = true;
				$timeout(function(){
					apter.tab = 4;
					var question_ouverte_img = document.getElementById("question_ouverte_image").getElementsByTagName('CANVAS');
					var data_question_ouverte_img = [];
					for(var i in question_ouverte_img){
						if(parseInt(i) || i ==0){
							/*console.log(i);
							console.log(question_img[i]);*/
							data_question_ouverte_img.push(question_ouverte_img[i].toDataURL());
						}
						
					}


					$timeout(function(){
						apter.tab = 3;
						var question_img = document.getElementById("chart_questions").getElementsByTagName('CANVAS');
						var data_question_img = [];
						for(var i in question_img){
							if(parseInt(i) || i ==0){
								/*console.log(i);
								console.log(question_img[i]);*/
								data_question_img.push(question_img[i].toDataURL());
							}
							
						}
						/*console.log(data_question_img);*/

						$timeout(function() {
							apter.tab = 2;
							var detail_img = document.getElementById("chart_details").getElementsByTagName('CANVAS');
							var data_detail_img = [];
							for(var i in detail_img){
								if(parseInt(i) || i ==0){
									//console.log(question_img[i]);
									data_detail_img.push(detail_img[i].toDataURL());
								}
								
							}
							//console.log(data_detail_img);

							$timeout(function() {
								apter.tab = 1;
								var principal_img = document.getElementById("chart_principales").getElementsByTagName('CANVAS');
								var data_principal_img = [];
								for(var i in principal_img){
									if(parseInt(i) || i ==0){
										//console.log(question_img[i]);
										data_principal_img.push(principal_img[i].toDataURL());
									}
									
								}
								//console.log(data_principal_img);

								var img_radar = document.getElementById("radar-chart").toDataURL("image/jpg");
								
								var data = {
									id : apter.ApterQuestionnaire.id,
									data_question_img: data_question_img,
									data_detail_img: data_detail_img,
									data_principal_img: data_principal_img,
									img_radar: img_radar,
									data_question_ouverte_img: data_question_ouverte_img,
								}
								$http({
									  method: 'POST',
									  url: ajaxurl,
									  data: {
									  	action : "admin_apter_questionnaires_ajax_export",
									  	data : data

									  }
									}).then(function successCallback(response) {
										apter.loading = false;
										
									    console.log(response);
									    if(response.data.generated){
									    	apter.rapport = true;
									    }
									    else{
									    	apter.generete_error = response.data.message;
									    }
									    
									  }, function errorCallback(response) {
									    // called asynchronously if an error occurs
									    // or server returns response with an error status.
									  });


							}, 2000);


						}, 2000);

					}, 2000);

				},2000)
					

				
				
			
			}
			apter.setTab = function(num){
				apter.tab = num;
			}

			apter.isSet = function(num){
				return apter.tab == num;
			}
			
			<?php if(isset($repondants)){
				?>
				apter.ApterRepondant = <?php echo json_encode($repondants)?>;
				for(var i in apter.ApterRepondant){
					apter.data.push(JSON.parse(apter.ApterRepondant[i].reponse));
					
				}

				<?php
			}

			?>

			var backgroundColor=  [
						                'rgba(255, 99, 132, 0.8)',
						                'rgba(54, 162, 235, 0.8)',
						                'rgba(255, 206, 86, 0.8)',
						                'rgba(75, 192, 192, 0.8)',
						                'rgba(153, 102, 255, 0.8)',
						                'rgba(255, 159, 64, 0.8)'
						            ];

			var iter = 0;
			var data_principal = [];
			var titre = "";
			/*console.log("apter.data");
			console.log(apter.data);*/

			for(var i in apter.ApterQuestionnaire.apter_pages){
				var data_detail = [];
				var page = apter.ApterQuestionnaire.apter_pages[i];
				
				for(var j in apter.ApterQuestionnaire.apter_pages[i].apter_questions){
					var question = apter.ApterQuestionnaire.apter_pages[i].apter_questions[j];
					apter.questions.push(question);
					var data_ouverte = [];
					if(question.type == "text"){
						for(var k in apter.data){
							if(apter.data[k] != null){

								if( apter.data[k].filter(function(elt) { return (elt.question_id == question.id )  && (elt.type =="text"); }).length > 0){
									data_ouverte.push(apter.data[k].filter(function(elt) { return (elt.question_id == question.id )  && (elt.type =="text"); })[0]);
									
									
								}
							}
								

							
						}

						apter.data_per_question_ouverte[iter] = data_ouverte.reduce(function(res, obj){
							if(!(obj.groupe in res) && ((typeof obj.text) != "undefined")){
								res[obj.groupe] = [];
								res[obj.groupe].push(obj.text);
								res["question_id"] = obj.question_id;
								res["titre"] = apter.ApterQuestionnaire.apter_pages[i].titre;
								res["sous_titre"] = apter.ApterQuestionnaire.apter_pages[i].sous_titre;
							}else{
								if((typeof obj.text) != "undefined"){
									res[obj.groupe].push(obj.text);
								}
								
							}
							return res;
						},{});

						

					}

				iter++;
				}

			}

			var iter = 0;
			page:
			for(var i in apter.ApterQuestionnaire.apter_pages){
				var data_detail = [];
				var page = apter.ApterQuestionnaire.apter_pages[i];

				
				for(var j in apter.ApterQuestionnaire.apter_pages[i].apter_questions){
					var question = apter.ApterQuestionnaire.apter_pages[i].apter_questions[j];
					var data = [];
					var data_ouverte = [];
					if(question.type == "text"){
						
						break page;
					}
					
					for(var k in apter.data){
						if(apter.data[k] != null){

							if( apter.data[k].filter(function(elt) { return (elt.question_id == question.id ); } ).length > 0){
								data.push(apter.data[k].filter(function(elt) { return (elt.question_id == question.id ); } )[0]);
								
								
							}
						}
							

						
					}
					var time = 0;

					
					

					apter.data_per_question[iter] = data.reduce(function(res, obj){
						
						if(!(obj.groupe in res)){
							var test = typeof res['none_titre'];
							res[obj.groupe+"_time"] = 1;
							if(typeof res['none_titre'] == "undefined"){
								res['none_titre'] = 0;
							}
							
							
							
							if(obj.score == 0){

									res[obj.groupe] =0;
									res["none_titre"] += 1; 
							}

						
							
							if(parseInt(obj.score)){
							
								res[obj.groupe] = parseInt(obj.score);

								res[obj.groupe+"_time"] = 1;
							}
							
							res["question_id"] = obj.question_id;
							res["titre"] = apter.ApterQuestionnaire.apter_pages[i].titre;
							res["sous_titre"] = apter.ApterQuestionnaire.apter_pages[i].sous_titre;
						}else{
							if(obj.score == 0){
								res['none_titre'] += 1;
							}
							if(parseInt(obj.score)){
								var temp = res[obj.groupe];
								res[obj.groupe] += parseInt(obj.score);
								if(temp != 0){
									res[obj.groupe+"_time"] +=1;
								}
							}else{
								res[obj.groupe] += 0;
							}
							
						}
						return res;
					},{});

				

					

					
					/*apter.data_per_question_ouverte[iter] = apter.data_per_question_ouverte[iter].filter(elt => (typeof elt.text) != "undefined");*/


					var labels = [];
					var chart_data = [];
					var data_to_page = {};
					console.log("apter.data_per_question[iter]");
					console.log(apter.data_per_question[iter]);
					for(var l in apter.data_per_question[iter]){
						if(!(l.includes("_time")) && !(l.includes("titre")) && !(l.includes("titre")) && l != "question_id" && l != "titre" ){
							labels.push(l);
							var val = apter.data_per_question[iter][l];
							if(apter.data_per_question[iter][l+"_time"]){
								val = apter.data_per_question[iter][l] /apter.data_per_question[iter][l+"_time"];
							}
							console.log("Iter : "+iter+", section"+l+ ", sous_titre : "+apter.data_per_question[iter]['sous_titre']+", val : "+val);
							chart_data.push(val);
							
							data_to_page[l] = val;
							data_to_page["titre"] = apter.data_per_question[iter]['titre'];
							data_to_page["sous_titre"] = apter.data_per_question[iter]['sous_titre'];
							data_to_page["none_titre"] = apter.data_per_question[iter]['none_titre'];
							
						}

					}
					data_detail.push(data_to_page);

					data_principal.push(data_to_page);
					var element = "question-chart-"+i+"-"+j;

					
					iter= iter+1;
				}



				titre = apter.ApterQuestionnaire.apter_pages[i].titre;

				console.log("data_detail");
				console.log(data_detail);
				
				apter.data_detail[i] = data_detail.reduce(function(res, obj){
						for(var k in obj){
							if(!(k in res)){
								res[k] = obj[k];

								if(!(k.includes("titre"))) res[k+"_time"] = 1;
							}else{
								/*if(parseInt(obj['none_titre']) || obj['none_titre'] ==0){
									res['none_titre'] += obj['none_titre'];
								}*/
								
								if(parseInt(obj[k])){
									 res[k] += obj[k];
									 res[k+"_time"] +=1;
								}
								
								
							}
						}
							
						return res;
					},{});

				/**/
				console.log("apter.data_detail["+i+"]");
				console.log(apter.data_detail[i]);


				
				


			}; 


			
			apter.data_principal = data_principal.reduce(function(res, obj){
				if(!(obj.titre in res)){
					for(var i in obj){
						if(!(i.includes('titre'))){
							obj[i+"_time"] = 1;
						}
						
					}
					res[obj.titre] = obj;

				}else{
					for(var i in obj){
						if(i =="none_titre"){
							res[obj.titre][i] += obj[i];
						}
						if(!(i.includes('titre'))){
							if(parseInt(obj[i])){
								res[obj.titre][i] += obj[i];
								res[obj.titre][i+"_time"] +=1;
							}
							
						}
					} 
				}
				return res;
			},[]);
			

			///////////////RADAR GRAPHS////////////////////////////////////////
			var element = "radar-chart";
					
			var title = document.createElement("h4");
			title.appendChild(document.createTextNode("Représentation radar"));
			var node = document.createElement("canvas");
			node.setAttribute("id",element);
			var background =['rgba(255, 99, 132, 0.8)',
						                'rgba(54, 162, 235, 0.8)',
						                'rgba(255, 206, 86, 0.8)',
						                'rgba(75, 192, 192, 0.8)',
						                'rgba(153, 102, 255, 0.8)',
						                'rgba(255, 159, 64, 0.8)'];
			var labels_radar = [];
			var data_radar = {};
			for(var j in apter.data_principal){
				labels_radar.push(j.match(/.{1,20}/g)[0]);
				for(var l in apter.data_principal[j]){
					if(!(l.includes("_time")) && !(l.includes("titre")) && l != "question_id" && l != "titre"){
						var to_push = {};
						to_push[l] = apter.data_principal[j][l] / apter.data_principal[j][l+"_time"] ;
						to_push["titre"] = apter.data_principal[j]["titre"];
						if(!data_radar[l]){
							data_radar[l] = [];
							data_radar[l].push(apter.data_principal[j][l] / apter.data_principal[j][l+"_time"]);
						}else{
							data_radar[l].push(apter.data_principal[j][l] / apter.data_principal[j][l+"_time"]);
						}
						//data_radar.push(to_push);
					}
				}
			}
			
			var data_radar_2 = Object.keys(data_radar).reduce(function(res, key){
				if(key == "VOUS"){
					if(!("VOUS" in res)){
						res["VOUS"] = data_radar[key];
					}
				}else{
						if(!("TOUS (HORS VOUS)" in res)){
							res["TOUS (HORS VOUS)"] = [];
							for(var i in data_radar[key]){
								res["TOUS (HORS VOUS)"][i] = data_radar[key][i];
								res["TOUS (HORS VOUS)"][i+"_time"] = 1;
							}
						}else{
							for(var i in data_radar[key]){
								res["TOUS (HORS VOUS)"][i] += data_radar[key][i];
								if(data_radar[key][i] != 0) res["TOUS (HORS VOUS)"][i+"_time"] += 1;
							}
						}
					}
				return res
			},{});
			/*console.log("data_radar_2 1");
			console.log(data_radar);*/
			for(var i in data_radar_2){
				if(i != "VOUS"){
					for(var j in data_radar_2[i]){
						data_radar_2[i][j] = data_radar_2[i][j] / data_radar_2[i][j+"_time"]
						delete data_radar_2[i][j+"_time"];
					}
				}
			}
			/*console.log("data_radar_2");
			console.log(data_radar_2);*/

			apter.data_radar = [];
			var k = 0;
			for(var i in data_radar_2){

				var to_push ={
					label: i,
					borderColor : background[k%background.length],
					backgroundColor : 'rgba(250,250,250,0)',
					data : data_radar_2[i]
				};
				k +=1;

				apter.data_radar.push(to_push);
			}

			
			
					
			document.getElementById("chart_radar").appendChild(title);
			document.getElementById("chart_radar").appendChild(node);
			
			var marksData = {
				  labels: labels_radar,
				  datasets: apter.data_radar
				};
				 
				var radarChart = new Chart(document.getElementById(element), {
				  type: 'radar',
				  data: marksData,
				  options: {
				  	scale:{
				  		ticks:{
				  			beginAtZero: true,
				  			max : 4,
				  			stepSize: 1
				  		}
				  	}
				  }
				});

			
			///////////////FIN RADAR GRAPHS////////////////////////////////////////


			////////////////MOYENNE PAR QUESION CHART/////////////////////////////////////
			
			
			var data_grouped = apter.data_per_question.reduce(function(res, obj){
					for(var l in obj){
						if(!(l.includes("_time")) && !(l.includes("titre")) && l != "question_id" && l != "titre" ){
							if(!(l in res)){
								res[l] = []
								res[l].push(obj[l] /obj[l+"_time"]);
							}else{
								res[l].push(obj[l] /obj[l+"_time"]);
							}
						}
					}
					return res;
				},{});

		
				

				var questions = [];
				
				var iter = 0;
				$timeout(function(){
					page2:
					for(var i in apter.ApterQuestionnaire.apter_pages){
						var page = apter.ApterQuestionnaire.apter_pages[i];
						var chartData = [];
						var labels = [];
						var data = [];
						var l =0;

						for(var o in page.apter_questions){
							if(page.apter_questions[o].type =="text"){
								break page2;
							}
						}

						for(var k in data_grouped){
								chartData.push({
									label: k,
									backgroundColor: backgroundColor[l%background.length] ,
									data: data_grouped[k].slice(iter,iter+apter.ApterQuestionnaire.apter_pages[i].apter_questions.length)
								});
								l++;
						}
						for(var j in apter.ApterQuestionnaire.apter_pages[i].apter_questions){
							var question = apter.ApterQuestionnaire.apter_pages[i].apter_questions[j];
							
							var lab =question.question.match(/.{1,80}/g);
							for (var m = 0; m < lab.length; m++) {
								if (m != lab.length - 1){
									lab[m]+="-";
								} 
							}
							lab.push("(Je ne sais pas : "+ apter.data_per_question[iter]['none_titre']+")");

						
							
							labels.push(lab);
							iter++;

						}

						
					

						for(var m in labels){
							var chartData_elt = [];
							for(var n in chartData){
								chartData_elt.push({
									label: chartData[n]["label"],
									backgroundColor: backgroundColor[n%background.length],
									data: [chartData[n]["data"][m]]
								});
							}
							

							var element = "question-chart-"+i+"-"+m;
							var node = document.createElement("canvas");
							node.setAttribute("id",element);
							document.getElementById("chart_questions").appendChild(node);
							var titre = page.titre + " | "+page.sous_titre;
							if(angular.isDefined(apter.titre_text) && apter.titre_text == page.titre + " | "+page.sous_titre){
								titre = "";
							}

							if(angular.isDefined(apter.titre_text) && apter.titre_text != page.titre + " | "+page.sous_titre){
								titre = page.titre + " | "+page.sous_titre;
							}
							apter.titre_text = page.titre + " | "+page.sous_titre;
							new Chart(document.getElementById(element), {
							    type: 'horizontalBar',
							    height:100,
							    data: {
							        labels: [labels[m]],
							        datasets: chartData_elt
							    },

							    options: {
							    	legend: {
								            display: false
								         },
								         tooltips: {
								            enabled: true
								         },
							    	animation: {
									    onComplete: function () {
									        var ctx = this.chart.ctx;
									        ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
									        ctx.fillStyle = "rgb(50,50,50)";
									        ctx.textAlign = 'center';
									        ctx.textBaseline = 'bottom';
									        ctx.font = '0.7em Verdana';


									        this.data.datasets.forEach(function (dataset)
									        {
									        	
									            for (var i = 0; i < dataset.data.length; i++) {
									                for(var key in dataset._meta)
									                {
									                    var model = dataset._meta[key].data[i]._model;

									                    ctx.fillText(dataset.label+" "+dataset.data[i].toFixed(2), model.x -40, model.y +6 );
									                }
									            }
									        });
									    }
									},

							    	title:{
								    		display: true,
								    		text : titre,
								    		fontSize: 14,
								    	},
								    
							    	maintainAspectRatio: true,
							    	responsive: true,

							    	
							        scales: {

							            yAxes: [{
							            	 afterFit: function(scaleInstance) {
											        scaleInstance.width = 500; // sets the width to 100px
											      },

							                ticks: {
							                    beginAtZero:true,
							                    
											     fontSize: 14,
											     stepSize: .1
							                   
							                }
							            }],
							            xAxes: [{
							                ticks: {
							                    beginAtZero:true,
							                    max : 4,
							                    
							                    
							                }
							            }]
							        }
							    }
							});
						}

							
					}
				},2000);
					
				////////////////FIN mOYENNE PAR QUESION CHART/////////////////////////////////////






			var data_radar = [];
			var labels_radar =[];

			////////////////////DIMENSION PRINCIPAL GRAPHS/////////////////////////
			var principale_labels = [];
			var principale_datas = [];
				for(var j in apter.data_principal){
					principale_labels.push(j);
					
					var chart_data = [];
					var labels = [];
					/*console.log("apter.data_principal[j]");
					console.log(apter.data_principal[j]);*/
					for(var l in apter.data_principal[j]){
						if(!(l.includes("_time")) && !(l.includes("titre")) && l != "question_id" && l != "titre" ){

							var val = apter.data_principal[j][l];
								if(apter.data_principal[j][l+"_time"]){
									val = apter.data_principal[j][l] /apter.data_principal[j][l+"_time"];
								}
							chart_data.push(val);
							var lab =l.match(/.{1,80}/g);
							for (var m = 0; m < lab.length; m++) {
								if (m != lab.length - 1){
									lab[m]+="-";
								} 
							}
							/*lab.push(" | Je ne sais pas : "+ apter.data_principal[j]['none_titre']);*/
							var none_response = apter.data_principal[j]['none_titre']
							labels.push(lab);
						}
						
					}
					data_radar.push({
						data : chart_data,
						label: '# de votes',
						backgroundColor: [
							                'rgba(255, 99, 132, 0.8)',
							                'rgba(54, 162, 235, 0.8)',
							                'rgba(255, 206, 86, 0.8)',
							                'rgba(75, 192, 192, 0.8)',
							                'rgba(153, 102, 255, 0.8)',
							                'rgba(255, 159, 64, 0.8)'
							            ],
							            borderColor: [
							                'rgba(255,99,132,1)',
							                'rgba(54, 162, 235, 1)',
							                'rgba(255, 206, 86, 1)',
							                'rgba(75, 192, 192, 1)',
							                'rgba(153, 102, 255, 1)',
							                'rgba(255, 159, 64, 1)'
							            ],
							            borderWidth: 1
					});
					if(labels_radar.length < labels.length){
						labels_radar = labels;
					}
					

					
					var element = "principale-chart-"+j;
						
						var title = document.createElement("h4");
						title.appendChild(document.createTextNode(j));
						var node = document.createElement("canvas");
						node.setAttribute("id",element);
						
						//document.getElementById("chart_principales").appendChild(title);
						document.getElementById("chart_principales").appendChild(node);

						
						new Chart(document.getElementById(element), {
							    type: 'horizontalBar',
							    data: {
							        labels: labels,
							        datasets: [{
							            label: '# de votes',
							            data: chart_data,
							            backgroundColor: [
							                'rgba(255, 99, 132, 0.8)',
							                'rgba(54, 162, 235, 0.8)',
							                'rgba(255, 206, 86, 0.8)',
							                'rgba(75, 192, 192, 0.8)',
							                'rgba(153, 102, 255, 0.8)',
							                'rgba(255, 159, 64, 0.8)'
							            ],
							            borderColor: [
							                'rgba(255,99,132,1)',
							                'rgba(54, 162, 235, 1)',
							                'rgba(255, 206, 86, 1)',
							                'rgba(75, 192, 192, 1)',
							                'rgba(153, 102, 255, 1)',
							                'rgba(255, 159, 64, 1)'
							            ],
							            borderWidth: 1
							        }]
							    },
							    options: {
							    	legend: {
							            display: false
							         },
							         tooltips: {
							            enabled: true
							         },
							    	animation: {
									    onComplete: function () {
									        var ctx = this.chart.ctx;
									        ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
									        ctx.fillStyle = "rgb(40,40,40)";
									        ctx.textAlign = 'center';
									        ctx.textBaseline = 'bottom';
									        ctx.font = '0.7em Verdana';

									        this.data.datasets.forEach(function (dataset)
									        {
									            for (var i = 0; i < dataset.data.length; i++) {
									                for(var key in dataset._meta)
									                {
									                    var model = dataset._meta[key].data[i]._model;
									                    ctx.fillText(dataset.data[i].toFixed(2), model.x -40, model.y+6 );
									                }
									            }
									        });
									    }
									},
							    	title:{
							    		display: true,
							    		text : j +" (Je ne sais pas : "+none_response+")",
							    		fontSize: 14,
							    	},
							    	maintainAspectRatio: true,
							        scales: {
							            yAxes: [{
							                ticks: {
							                    beginAtZero:true,
							                    fontSize: 14,
							                },
							                afterFit: function(scaleInstance) {
										        scaleInstance.width = 150; // sets the width to 100px
										      }
							            }],
							            xAxes: [{
							                ticks: {
							                    beginAtZero:true,
							                    max : 4
							                }
							            }]
							        }
							    }
							});
							/*node.setAttribute("style","display: block; height: 100px !important;");*/
					
				}

			////////////////////FIN DIMENSION PRINCIPAL GRAPHS/////////////////////////



			////////////////DETAIL CHART/////////////////////////////////////
			$timeout(function(){
					console.log('apter.data_detail');
					console.log(apter.data_detail);
					apter.data_detail_grouped = apter.data_detail.reduce(function(res, obj){
							if(!(obj.titre in res)){
								res[obj.titre] = [];
								res[obj.titre].push(obj);
							}else{
								res[obj.titre].push(obj);
							}
							return res;
						},{});
						var titre = "";


						for(var i in apter.data_detail_grouped){
							var labels = [];
							var chartData = [];
							
							for(var j in apter.data_detail_grouped[i]){
								var cur = apter.data_detail_grouped[i][j];
								var lab =cur.sous_titre.match(/.{1,40}/g);
								for (var m = 0; m < lab.length; m++) {
									if (m != lab.length - 1){
										lab[m]+="-";
									} 
								}
								lab.push( "(Je ne sais pas : "+cur.none_titre+")");
								labels.push(lab);
								titre = cur.titre


							}
							var per_group = apter.data_detail_grouped[i].reduce(function(res, obj){
								for(var k in obj){
									if(!(k.includes('titre')) && !(k.includes('_time'))){
										if(!(k in res)){
											res[k] = [];
											var val = obj[k] /obj[k+"_time"];
											res[k].push(val);
										}else{
											var val = obj[k] /obj[k+"_time"];
											res[k].push(val);
										}
									}
								}
								return res;
							},{});
							var m =0;
							
							for(var l in per_group){
								chartData.push({
										label: l,
										backgroundColor: backgroundColor[m%backgroundColor.length],
										data: per_group[l]
								});
								m++;
							}
							
							
							

							for(var m in labels){
								var chartData_elt = [];
								for(var n in chartData){
									chartData_elt.push({
										label: chartData[n]["label"],
										backgroundColor: backgroundColor[n%background.length],
										data: [chartData[n]["data"][m]]
									});
								}

								var titre2 = titre;
								if(angular.isDefined(apter.titre_text_detail) && apter.titre_text_detail == titre){
									titre2 = "";
								}

								if(angular.isDefined(apter.titre_text_detail) && apter.titre_text_detail != titre){
									titre2 = titre;
								}
								apter.titre_text_detail = titre;

								var element = "detail-chart-"+i+"-"+m;
								var node = document.createElement("canvas");
								node.setAttribute("id",element);
								document.getElementById("chart_details").appendChild(node);
								console.log("chartData_elt");
								console.log(chartData_elt);

								new Chart(document.getElementById(element), {
								    type: 'horizontalBar',
								    data: {
								        labels: [labels[m]],
							        	datasets: chartData_elt
								    },
								    options: {
								    	legend: {
								            display: false
								         },
								         tooltips: {
								            enabled: true
								         },
								    	animation: {
										    onComplete: function () {
										        var ctx = this.chart.ctx;
										        ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
										        ctx.fillStyle = "rgb(40,40,40)";
										        ctx.textAlign = 'center';
										        ctx.textBaseline = 'bottom';
										        ctx.font = '0.7em Verdana';

										        this.data.datasets.forEach(function (dataset)
										        {
										        	
										            for (var i = 0; i < dataset.data.length; i++) {
										                for(var key in dataset._meta)
										                {
										                    var model = dataset._meta[key].data[i]._model;
										                    ctx.fillText(dataset.label+" "+dataset.data[i].toFixed(2), model.x -40, model.y+6 );
										                }
										            }
										        });
										    }
										},
								    	title:{
									    		display: true,
									    		text : titre2 ,
									    		fontSize: 14,
									    	},
									    
								    	maintainAspectRatio: true,
								    	
								        scales: {
								            yAxes: [{
								                ticks: {
								                    beginAtZero:true,
								                    fontSize: 14,
								                    
								                  
								                },
								                afterFit: function(scaleInstance) {
											        scaleInstance.width = 350; // sets the width to 100px
											      }
								            }],
								            xAxes: [{
								                ticks: {
								                    beginAtZero:true,
								                    max : 4
								                }
								            }]
								        }
								    }
								});

							}

								
						}

			},2000);
					
				//////////////// FIN DETAIL CHART/////////////////////////////////////
				

				/*var element = "question_ouverte_elt";
				var node = document.createElement("canvas");
				node.setAttribute("id",element);*/
/*
				console.log(apter.data_per_question_ouverte);*/

				for(var i in apter.data_per_question_ouverte){
					var data = apter.data_per_question_ouverte[i];
					var question = document.createElement('h1');
					
					var question_text = apter.questions.filter(function(elt) { return elt.id == data.question_id;})[0];
					if(typeof question_text != "undefined"){
						question.appendChild(document.createElement('b').appendChild(document.createTextNode(question_text.question)))
						document.getElementById("question_ouverte").appendChild(question);
						for(var j in data){
							if(!(j.includes('titre')) && j != "question_id"){
								var groupe = document.createElement('h3');
								var reponse = document.createElement('h1');
								groupe.appendChild(document.createTextNode(j));
								document.getElementById("question_ouverte").appendChild(groupe);
								rep = "";

								for(var k in data[j]){
									rep+= data[j][k];
									if(k != data[j].length -1){
										rep+=",";
									}
								}
								reponse.appendChild(document.createTextNode(rep));
								document.getElementById("question_ouverte").appendChild(reponse);

							}
						}
					} 
					

				}
				    

				
			    //document.getElementById("question_ouverte").appendChild(node);

			
			
			


		});

	app.config(function ($httpProvider, $httpParamSerializerJQLikeProvider){
	    $httpProvider.defaults.transformRequest.unshift($httpParamSerializerJQLikeProvider.$get());
	    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=utf-8';
	});


	$(document).ready(function(){
		html2canvas(document.getElementById("question_ouverte"), {
				      onrendered: function(canvas) {
				        document.getElementById("question_ouverte_image").appendChild(canvas);
				      }
				    });
	});


</script>