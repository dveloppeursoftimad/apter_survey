<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script type="text/javascript" src="<?php echo mvc_js_url("apter-survey","jquery") ?>"></script>
<script type="text/javascript">
	$(document).ready(function() {
	    tinyMCE.init({
	        mode : "none",
	        height : "500"
	    });
	    tinyMCE.execCommand('mceAddEditor', false, 'ApterMailMail');

	    $("label").hide();
	});
</script>

<h2>Modifier Mail : <?php echo $object->titre ?></h2>

<?php echo $this->form->create($model->name); ?>
<?php echo $this->form->input('mail'); ?>
<br>
<input type="submit" class="button button-primary" name="" value="Modifier">
</form>

