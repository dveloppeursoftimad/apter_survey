<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script type="text/javascript" src="<?php echo mvc_js_url("apter-survey","jquery") ?>"></script>

<h2>Ajout Mail</h2>

<?php echo $this->form->create($model->name); ?>
<?php echo $this->form->input('mail'); ?>
<?php echo $this->form->input('type'); ?>
<?php echo $this->form->input('titre'); ?>
<br>
<input type="submit" class="button button-primary" name="" value="Ajouter">
</form>

<script type="text/javascript">
	$(document).ready(function() {
	    tinyMCE.init({
	        mode : "none"
	    });
	    tinyMCE.execCommand('mceAddEditor', false, 'ApterMailMail');

	    $("label").hide();
	});
</script>