<?php

class ApterQuestionnaire extends MvcModel {

    var $display_field = 'nom';
    var $has_many = array('ApterPage'=> array(
      'dependent' => true
    ), 'ApterQuestion'=> array(
      'dependent' => true
    ), 'ApterRepondant'=> array(
      'dependent' => true
    ), 'ApterGroupe'=> array(
      'dependent' => true
    ));

    var $belongs_to = array(
	    'ApterCc' => array(
        'foreign_key' => 'consultant_id',
        'dependent' => false
	    )
	  );
    var $includes = array('ApterPage');
    
}

?>