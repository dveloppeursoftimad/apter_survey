<?php

class ApterPage extends MvcModel {

    var $display_field = 'nom';
    var $belongs_to = array('ApterQuestionnaire');
    var $has_many = array('ApterQuestion' => array(
      'dependent' => true
    ));
    var $includes = array('ApterQuestion');
    
}

?>