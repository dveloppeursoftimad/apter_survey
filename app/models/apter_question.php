<?php

class ApterQuestion extends MvcModel {

    var $display_field = 'nom';
    var $belongs_to = array('ApterQuestionnaire', 'ApterPage');
    var $has_many = array('ApterReponse'=> array(
      'dependent' => true
    ));
    var $includes = array('ApterReponse');

    
}

?>