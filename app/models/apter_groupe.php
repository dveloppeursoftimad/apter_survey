<?php

class ApterGroupe extends MvcModel {

    var $display_field = 'name';

    var $has_many = array(
	    'ApterRepondant' => array(
	      'dependent' => true
	    )
	  ); 
    
}

?>