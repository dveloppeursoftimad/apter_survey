<?php

class ApterRepondant extends MvcModel {

    var $display_field = 'name';
    var $belongs_to = array('ApterQuestionnaire'=> array(
      'dependent' => true,
      'foreign_key' => 'apter_questionnaire_id',
      /*
      array('ApterCc' => array(
        'dependent' => false,
        'foreign_key' => 'consultant_id',
      ))*/
    ),'ApterGroupe'=> array(
      'dependent' => true
    ));
    
}

?>