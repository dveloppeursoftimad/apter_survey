<?php

class ApterQuestionnairesController extends MvcPublicController {
 	public function show(){

 		$q =  $this->ApterQuestionnaire->find_by_id($this->params['id'], array(
			'includes' => array('ApterPage' => ['includes' => ['ApterQuestion' => ['selects' => 'ApterQuestion.*']],'selects' => 'ApterPage.*'], 'ApterRepondant' => ['selects' => 'ApterRepondant.*','includes' => ['ApterGroupe' => ['selects' => 'ApterGroupe.*']]])
			));
 		$q_ouvertes = [];

 			foreach ($q->apter_pages as $key => $page) {
 				foreach ($page->apter_questions as $key => $question) {
 					if($question->type =="text"){
	 						$q_ouvertes[] = $question;
	 				}
 				}
 			}
 		$repondantModel = mvc_model("ApterRepondant");
 		$repondants = $repondantModel->find([
	    			'includes' => ['ApterGroupe' => ['selects'=> 'ApterGroupe.*']],
	    			'conditions' => [
	    				'ApterRepondant.apter_questionnaire_id' => $this->params['id']
	    			]	
	    		]);
 		$q_ouvertes_grouped= [];

 		$q_o_result = [];
 		//for each quest ouvertes get grouped result
 		foreach ($q_ouvertes as $key => $question) {
 			$q_ouvertes_grouped[$question->id] = "";
 			foreach ($repondants as $key => $value) {
		    		/*echo "<pre>";
	 		var_dump( json_decode($value->reponse));
	 		echo "</pre>";*/

		    			$reponse = json_decode($value->reponse);
		    			if($reponse){
		    				/*echo $question->id;
		    				echo "<pre>";
					 		var_dump( $reponse);
					 		echo "</pre>";*/
		    				
		    					$reponse = array_filter($reponse, function($v){
				    				return "".$v->question_id == "".$question->id;
				    			}, ARRAY_FILTER_USE_BOTH);
				    			echo "<pre>";
						 		var_dump($reponse);
						 		echo "</pre>";
				    			if($reponse->question_id == $question->id){
				    				echo "here";
				    				if(!isset($groupes[$value->apter_groupe->abreviation])){
					    				$q_ouvertes_grouped[$question->id] .="<b>".$value->apter_groupe->abreviation."</b><w:br/>\n";
					    				$groupes[$value->apter_groupe->abreviation]["nb_reponses"] =1;
					    			}else{
					    				$q_ouvertes_grouped[$question->id] .= 
					    				$groupes[$value->apter_groupe->abreviation]["nb_reponses"]+=1;
					    				$q_ouvertes_grouped[$question->id] .=$reponse->text."<w:br/>\n";
					    			}

				    			}
		    			}
				    			
		    

		    }
 		}

 		// echo "<pre>";
	 	// 	var_dump($q_ouvertes_grouped);
	 	// 	echo "</pre>";

	 		


 		
 		// die();
		if(!empty($this->params['id'])){
			$id = $this->params['id'];
			//echo $id."id";
			$groupeModel = mvc_model('ApterGroupe');
			$objects =  $this->ApterQuestionnaire->find_by_id($id, array(
			'includes' => array('ApterPage' => ['includes' => ['ApterQuestion' => ['selects' => 'ApterQuestion.*']],'selects' => 'ApterPage.*'], 'ApterRepondant' => ['selects' => 'ApterRepondant.*','includes' => ['ApterGroupe' => ['selects' => 'ApterGroupe.*']]])
			));
			/*echo "<pre>";
			var_dump($objects);
			echo "</pre>";*/
			$groupes = $groupeModel->find([
				'conditions' => [
					'ApterGroupe.apter_questionnaire_id' => $id
				],
				'group' => "ApterGroupe.abreviation"
			]);

			$this->set("objects", $objects);
			$this->set("id", $id);
			$this->set("groupes", $groupes);
			$this->render_view('apter_questionnaires/show', array('layout' => 'apter_layout'));
		}
	}

	public function login(){
		
	}


   
}

?>