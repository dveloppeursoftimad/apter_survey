<?php

//Install php Excel

//include( mvc_plugin_app_path('apter-survey').'libs/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php');
//require_once( mvc_plugin_app_path('apter-survey').'libs/vendor/autoload.php');

class AdminApterQuestionnairesController extends MvcAdminController {
    
    var $default_columns = array('id', 'type');

    public function index(){
		/*$this->params['page'] = empty($this->params['page']) ? 1 : $this->params['page'];
    	$this->params["conditions"] =  array('consultant_id' => get_current_user_id());
    

    	$collection = $this->model->paginate($this->params);
	    $this->set('objects', $collection['objects']);
	    $this->set_pagination($collection);*/


        /*$this->params['conditions'] = array('is_public' => true);
        */
        $this->init_default_columns();
        $this->process_params_for_search();
        /*$this->params['conditions'] = array('consultant_id' => get_current_user_id());*/
        $this->params['order'] = 'date DESC';
        $collection = $this->model->paginate($this->params);
        $this->set('objects', $collection['objects']);
        $this->set_pagination($collection);
    }

    public function reponses(){

    	$repondants = mvc_model('ApterRepondant')->find();
    	/*foreach ($repondants as $key => $value) {
    		
    		if($value->reponse){
    			$rep = (array) json_decode($value->reponse);
	    		foreach ($rep as $key2 => $value2) {
	    			if($value2->score =="5" || $value2->score == 5){
	    				
	    				$value2->score = 0;
	    				
		    			mvc_model('ApterRepondant')->update($value->id, array('reponse' => json_encode($rep)));
			    		
	    			}
	    		}
    		}
	    	
    	}
    	echo "OKOK";
    	die();*/
    	if(!empty($this->params['id'])){
    		$id = $this->params['id'];
    		$objects = 	$this->ApterQuestionnaire->find_by_id($id, array(
    			'includes' => array('ApterPage' => ['includes' => ['ApterQuestion' => ['selects' => 'ApterQuestion.*']],'selects' => 'ApterPage.*'])
	    	));
    		
	    	
	    	$this->set("objects", $objects);
    		$repondantModel = mvc_model('ApterRepondant');
	    	$repondants = $repondantModel->find_by_apter_questionnaire_id($id);
	    	$this->set("repondants", $repondants);
    	}

    }

    public function matrice(){
    	$target_dir = mvc_plugin_app_path('apter-survey')."public/templates/";
    	if(!empty($_FILES['rapport']) && $_FILES['rapport']['size'] != 0){
    	
    		
    			$type_file = $_FILES['rapport']['type'];
    			
			    $tmp_file = $_FILES['rapport']['tmp_name'];
			    $name_file = "Template360FBML.docx";

			    /*echo $name_file;
			    die();*/

			    if( !is_uploaded_file($tmp_file) )
			    {
			    	$this->flash('error', 'Le fichier est introuvable');
			    }else if( $type_file != 'application/vnd.openxmlformats-officedocument.wordprocessingml.document')
			    {
			    	$this->flash('error', 'Le fichier uploadé n\'est pas un fichier Word');
			    }else if( !move_uploaded_file($tmp_file, $target_dir.$name_file) )
			    {
			    	$this->flash('error',"Impossible de copier le fichier dans /uploads");
			    }else{
			    	$this->flash('notice',"Fichier uploadé avec succès");
			    }
    	}
    		
    		//$template = new \PhpOffice\PhpWord\TemplateProcessor($template_dir.'Template360FBML.docx');

    }

    public function edit(){
    	if(!empty($this->params['id'])){
    		$id = $this->params['id'];
    		$objects = 	$this->ApterQuestionnaire->find_by_id($id, array(
    			'includes' => array('ApterPage' => ['includes' => ['ApterQuestion' => ['selects' => 'ApterQuestion.*']],'selects' => 'ApterPage.*'])
	    	));
    		
	    	
	    	$this->set("objects", $objects);
	    	$this->set("id", $id);
	    	$reponse = false;
	    	$repondantModel = mvc_model('ApterRepondant');
	    	$repondants = $repondantModel->find_by_apter_questionnaire_id($id);
	    	foreach ($repondants as $key => $value) {

	    		if($value->reponse != NULL || $value->reponse != ""){
	    			$reponse = true;
	    		}
	    	}
	    	$this->set("reponse", $reponse);

    	}
    	
    	
    }

    public function supprimer(){
    	$this->set_object();
	    if (!empty($this->object) && !empty($this->params['id'])) {
	      $this->ApterQuestionnaire->delete($this->params['id']);
	      $this->flash('notice', 'Suppression réussi!');
	    } else {
	      $this->flash('warning', 'Le test choisi n\'existe pas.');
	    }    		
		$url = MvcRouter::admin_url(array('controller' => 'admin_apter_questionnaires', 'action' => 'index'));
		$this->redirect($url);
    }

    public function analyse(){
    	if(!empty($this->params['id'])){
    		$id = $this->params['id'];
    		$objects = 	$this->ApterQuestionnaire->find_by_id($id, array(
    			'includes' => array('ApterPage' => ['includes' => ['ApterQuestion' => ['selects' => 'ApterQuestion.*']],'selects' => 'ApterPage.*'])
	    	));
	    	$repondantModel = mvc_model('ApterRepondant');
	    	$repondants = $repondantModel->find_by_apter_questionnaire_id($id);
	    	$nb_reponse = 0;
	    	foreach ($repondants as $key => $value) {
	    		if($value->reponse != NULL && $value->reponse != ""){
	    			$nb_reponse++;
	    		}
	    	}
	    	$this->set('repondants', $repondants);
	    	$this->set('nb_reponse', $nb_reponse);
	    	$this->set("objects", $objects);
	    	$this->set("id", $id);
    	}
    }

    public function export(){
    		$template_dir = mvc_plugin_app_path('apter-survey')."public/templates/";
    		$target_dir = mvc_plugin_app_path('apter-survey')."public/uploads/1/";
    		
		
    }

    public function ajax_export(){
    	$reponse = ["message" => "Une erreur est survenu lors de l'exportation", "generated" => false];
    	
    	if(!empty($this->params['data']) && !empty($this->params['data']['id']) && !empty($this->params['data']['data_question_img']) && !empty($this->params['data']['data_detail_img']) && !empty($this->params['data']['data_principal_img']) && !empty($this->params['data']['img_radar'])   ){
    		$repondantModel = mvc_model("ApterRepondant");

    		$id = $this->params['data']['id'];

    		$quest = $this->ApterQuestionnaire->find_by_id($id);

    		$q =  $this->ApterQuestionnaire->find_by_id($this->params['id'], array(
			'includes' => array('ApterPage' => ['includes' => ['ApterQuestion' => ['selects' => 'ApterQuestion.*']],'selects' => 'ApterPage.*'], 'ApterRepondant' => ['selects' => 'ApterRepondant.*','includes' => ['ApterGroupe' => ['selects' => 'ApterGroupe.*']]])
			));
 			$q_ouvertes = [];

 			foreach ($q->apter_pages as $key => $page) {
 				foreach ($page->apter_questions as $key => $question) {
 					if($question->type =="text"){
	 						$q_ouvertes[] = $question;
	 				}
 				}
 			}




    		$consultant = wp_get_current_user();
    		$repondants = $repondantModel->find([
	    			'includes' => ['ApterGroupe' => ['selects'=> 'ApterGroupe.*']],
	    			'conditions' => [
	    				'ApterRepondant.apter_questionnaire_id' => $id
	    			]	
	    		]);
    		$nb_repondants = count($repondants);

	    	$nb_reponses = 0;
	    	$groupes =[];
	    	
	    	foreach ($repondants as $key => $value) {
	    		
	    		if($value->reponse != NULL && $value->reponse != ""){
	    			$nb_reponses++;
	    			
	    			if(!isset($groupes[$value->apter_groupe->abreviation])){

	    				$groupes[$value->apter_groupe->abreviation]["nb_reponses"] =1;
	    			}else{
	    				$groupes[$value->apter_groupe->abreviation]["nb_reponses"]+=1;
	    			}

	    		}

	    	}
	    	

	    	foreach ($repondants as $key => $value) {
	    		if(!isset($groupes[$value->apter_groupe->abreviation]["nb"])){
	    				$groupes[$value->apter_groupe->abreviation]["nb"] =1;
	    				$groupes[$value->apter_groupe->abreviation]["nom"] = $value->apter_groupe->nom;
	    			}else{
	    				$groupes[$value->apter_groupe->abreviation]["nb"]+=1;
	    		}
	    	}

	    	
	    	$groupe_string = "";
	    	foreach ($groupes as $key => $value) {
	    		$groupe_string.=$value["nom"]." (".$key.") : ".$value["nb_reponses"]." / ".$value["nb"]."<w:br/>\n";
	    	}

	    	$taux_reponse = ($nb_reponses / $nb_repondants)*100;

			

    		$datas = $this->params['data'];
    		$target_dir = mvc_plugin_app_path('apter-survey')."public/uploads/";
    		if (!file_exists($target_dir.$datas['id']."/")) {
			    mkdir($target_dir.$datas['id']."/", 0777, true);
			}
			$target_dir = $target_dir.$datas['id']."/";

			$template_dir = mvc_plugin_app_path('apter-survey')."public/templates/";

    		
    		$template = new \PhpOffice\PhpWord\TemplateProcessor($template_dir.'Template360FBML.docx');
			$template->setValue('ben_initiales', $quest->ben_initiales);
			$template->setValue('ben_societe', $quest->ben_societe);
			$template->setValue('cons_firstname', $consultant->user_firstname);
			$template->setValue('cons_lastname', $consultant->user_lastname);

			setlocale (LC_TIME, 'fr_FR.utf8','fra');
			
			$template->setValue('date', strftime("%A %d %B %Y."));
			//strftime("%A %d %B %Y.")   date("D M d, Y")
			$template->setValue('nb_reponses', $nb_reponses);
			$template->setValue('nb_repondants', $nb_repondants);
			$template->setValue('taux_reponse', round($taux_reponse));
			$template->setValue('ben_civilite', $quest->ben_civilite);
			$template->setValue('ben_firstname', $quest->ben_firstname);
			$template->setValue('ben_lastname', $quest->ben_lastname);
			$template->setValue('quest_type', htmlspecialchars($quest->type) );
			$template->setValue('groupesArray', $groupe_string);


			
			$template->cloneRow('question_img_array', count($datas['data_question_img']));
			
    		
			//save questimage
			$i = 1;
			foreach ($datas['data_question_img'] as $key => $value) {
				$imageData = $value;
				$filteredData=substr($imageData, strpos($imageData, ",")+1);
	    		$unencodedData=base64_decode($filteredData);
	    		
	    		file_put_contents($target_dir."question_img_".$key.".png", $unencodedData);

	    		$template->setImg('question_img_array#'.($i),array('src' => $target_dir."question_img_".$key.".png",'swh'=>'580'));
	    		$i++;
			}

			
			$template->cloneRow('detail_img_array', count($datas['data_detail_img']));
			
			$i = 1;
			//save datail img
			foreach ($datas['data_detail_img'] as $key => $value) {
				$imageData = $value;
				$filteredData=substr($imageData, strpos($imageData, ",")+1);
	    		$unencodedData=base64_decode($filteredData);
	    		
	    		file_put_contents($target_dir."detail_img_".$key.".png", $unencodedData);
	    		$template->setImg('detail_img_array#'.($i),array('src' => $target_dir."detail_img_".$key.".png",'swh'=>'550'));
	    		
	    		$i++;
			}

			$template->cloneRow('principal_img_array', count($datas['data_principal_img']));
			$i=1;
			//save principal img
			foreach ($datas['data_principal_img'] as $key => $value) {
				$imageData = $value;
				$filteredData=substr($imageData, strpos($imageData, ",")+1);
	    		$unencodedData=base64_decode($filteredData);
	    		
	    		file_put_contents($target_dir."principal_img_".$key.".png", $unencodedData);
	    		$template->setImg('principal_img_array#'.($i),array('src' => $target_dir."principal_img_".$key.".png",'swh'=>'550'));
	    		$i++;
			}

			//save question ouverte img
			///GET question ouverte

			$template->cloneRow('question_ouverte_img_array', count($datas['data_question_ouverte_img']));

			$i=1;
			//save principal img
			foreach ($datas['data_question_ouverte_img'] as $key => $value) {
				$imageData = $value;
				$filteredData=substr($imageData, strpos($imageData, ",")+1);
	    		$unencodedData=base64_decode($filteredData);
	    		
	    		file_put_contents($target_dir."question_ouverte_img_".$key.".png", $unencodedData);
	    		$template->setImg('question_ouverte_img_array#'.($i),array('src' => $target_dir."question_ouverte_img_".$key.".png",'swh'=>'580'));
	    		$i++;
			}
			//save radar img
			$imageData = $datas['img_radar'];
			$filteredData=substr($imageData, strpos($imageData, ",")+1);
	    	$unencodedData=base64_decode($filteredData);
	    		
	    	file_put_contents($target_dir."radar_img.png", $unencodedData);
	    	$template->setImg('radar_img',array('src' => $target_dir."radar_img.png",'swh'=>'550'));


    		
	    	
			
			/*$i = 1;
			foreach ($datas['data_question_img'] as $key => $value) {
				$template->setImg('question_img_array#'.($i),array('src' => $target_dir."question_img_".$key.".png",'swh'=>'550'));
				$i = $i+1;
			}*/

			$template->saveAs($target_dir."Rapport_".$quest->type.".docx");
			if(file_exists($target_dir."Rapport_".$quest->type.".docx")){
				$reponse["generated"] = true;
			}
			


    	}
    	echo json_encode($reponse);
    	die();
    }

    public function ajax_validate_repondant(){
    	$reponse = [
    		'message' => "Les informations entrées ne sont pas valides",
    		'repondant' => NULL
    		];
    	if(!empty($this->params['data']) && !empty($this->params['data']['apter_groupe_id']) && !empty($this->params['data']['code']) && !empty($this->params['data']['lastname']) && !empty($this->params['data']['firstname']) && !empty($this->params['data']['id'])){
    		$repondantModel = mvc_model("ApterRepondant");
    		$id = $this->params['data']['id'];
    		$ben = $this->ApterQuestionnaire->find_by_id($id);

    			$repondant = $repondantModel->find([
	    			'includes' => ['ApterGroupe' => ['selects'=> 'ApterGroupe.*']],
	    			'conditions' => [
	    				'ApterRepondant.apter_groupe_id' => $this->params['data']['apter_groupe_id'],
	    				'ApterRepondant.code' => $this->params['data']['code'],
	    				'ApterRepondant.apter_questionnaire_id' => $this->params['data']['id'],
	    				'LOWER(ApterRepondant.firstname)' => strtolower($this->params['data']['firstname']),
	    				'LOWER(ApterRepondant.lastname)' => strtolower($this->params['data']['lastname'])
	    			]	
	    		]);
	    		if($repondant){
	    			$reponse['repondant'] = $repondant[0];
	    			$reponse['message'] = "";
	    		}
    		

    		


	    		
    		
    	}
    	echo json_encode($reponse);
    	die();
    }

    public function ajax_add_result(){
    	if(!empty($this->params['data']) && !empty($this->params['data']['reponse']) && !empty($this->params['data']['repondant_id']) && !empty($this->params['data']['id'])){
    		$repondantModel = mvc_model('ApterRepondant');
    		$repondant = $repondantModel->find_by_id($this->params['data']['repondant_id']);
    		$repondantModel->update($this->params['data']['repondant_id'], ["reponse" => $this->params['data']['reponse']]);
    		var_dump($repondant->reponse == NULL);
    		if($repondant->reponse == NULL){
    			echo "ato";
    			$quest = $this->ApterQuestionnaire->find_by_id($this->params['data']['id']);
    			$this->ApterQuestionnaire->update($this->params['data']['id'], ['nb_resultat' => $quest->nb_resultat + 1]);
    			echo "updated";
    		}

    	}
    	die();
    }

    public function collecte(){
    	if(!empty($this->params['id'])){
    		$this->set('id', $this->params['id']);
    		$id = $this->params['id'];
    		
    		

    		
    		$this->set_object();
    		if(!empty($_FILES['apter-file'])){
    			$target_dir = mvc_plugin_app_path('apter-survey')."public/uploads/";
    			$content_dir = 'upload/'; // dossier où sera déplacé le fichier
    			$type_file = $_FILES['apter-file']['type'];
    			
			    $tmp_file = $_FILES['apter-file']['tmp_name'];
			    $name_file = $_FILES['apter-file']['name'];

			    if( !is_uploaded_file($tmp_file) )
			    {
			    	$this->flash('error', 'Le fichier est introuvable');
			    }else if( !strstr($type_file, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') && !strstr($type_file, 'application/vnd.ms-excel') )
			    {
			    	$this->flash('error', 'Le fichier n\'est pas un fichier Excel');
			    }else if( !move_uploaded_file($tmp_file, $target_dir.$name_file) )
			    {
			    	$this->flash('error',"Impossible de copier le fichier dans /uploads");
			    }else{
			    	$inputFileName = $target_dir.$name_file;

					/*check point*/

					$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
					$objReader = PHPExcel_IOFactory::createReader($inputFileType);
					$objPHPExcel = $objReader->load($inputFileName);

					$data = array(1,$objPHPExcel->getActiveSheet()->toArray(null,true,true,true));

					//print the result
					$beneficiaire = [];
					$repondants = [];
					$data_valid = true;
					$start = false;
					if($data[0]==1){
						foreach($data[1] AS $row){
							foreach($row AS $key => $column){
								if($column == 'Civilité :' && !$beneficiaire['civilite']){
									$beneficiaire['ben_civilite'] = $row[strtoupper(++$key)];
								}
								if($column == "Prénom :" && !$beneficiaire['firstname']){
									$beneficiaire['ben_firstname'] = $row[strtoupper(++$key)];
								}
								if($column == "NOM :"  && !$beneficiaire['lastname']){
									$beneficiaire['ben_lastname'] = $row[strtoupper(++$key)];
								}
								if($column == "Société :"  && !$beneficiaire['societe']){
									$beneficiaire['ben_societe'] = $row[strtoupper(++$key)];
								}
								if($column == "Initiales"  && !$beneficiaire['initiales']){
									$beneficiaire['ben_initiales'] = $row[strtoupper(++$key)];
								}
								if($column == "e-mail :"  && !$beneficiaire['email']){
									$beneficiaire['ben_email'] = $row[strtoupper(++$key)];
									if(!filter_var($beneficiaire['ben_email'], FILTER_VALIDATE_EMAIL)){
										$beneficiaire['ben_email_error'] = "Ce mail n'est pas valide";
										$data_valid = false;
									}
								}
			
								if($column == "langue :"  && !$beneficiaire['langue']){
									$beneficiaire['ben_langue'] = $row[strtoupper(++$key)];
								}
								if($column == "Tél. portable :"  && !$beneficiaire['tel']){
									$beneficiaire['ben_tel'] = $row[strtoupper(++$key)];
								}

								if($column == "Légende"){
									
									$start = false;
									break;
								}

								if($start && $row['B'] == "" ){
									
									break;
								}
								if($start && $row['B'] != "" && $row['B'] != "Légende"){
									$repondant = [];
									$k = ++$key;
									$repondant['groupe'] =$row[$k];
									
									if($repondant['groupe'] == ""){
										break;
									}								
									$k = strtoupper(++$k);
									$repondant['abrev'] =$row[$k];
									
									if($repondant['abrev'] == ""){
										$repondant['abrev_error'] = "Ce champ est requis";
										$data_valid = false;
									}
									$k =++$k;
									$repondant['civilite'] =$row[$k];
									$k =++$k;
									$repondant['firstname'] =$row[$k];
									if($repondant['firstname'] == ""){
										$repondant['firstname_error'] = "Ce champ est requis";
										$data_valid = false;
									}
									$k =++$k;
									$repondant['lastname'] =$row[$k];
									if($repondant['lastname'] == ""){
										$repondant['lastname_error'] = "Ce champ est requis";
										$data_valid = false;
									}
									$k =++$k;
									$repondant['email'] =$row[$k];
									
									if(!filter_var($repondant['email'], FILTER_VALIDATE_EMAIL)){
										$repondant['email_error'] = "Adresse mail non valide";
										$data_valid = false;
									}
									$repondants[] = $repondant;
									break;
								}


								if($column == "nom du groupe de répondants (1)"){
									$start = true;
									break;
								}
								



							}
						}
					}
					/*echo "<pre>";
					var_dump($beneficiaire);
					echo "</pre>";
					echo "<pre>";
					var_dump($repondants);
					echo "</pre>";*/
					$this->set('beneficiaire', $beneficiaire);
					$this->set('repondants', $repondants);
					$this->set('data_valid', $data_valid);
					$this->set('data_import', true);
					$this->flash('notice',"Le fichier a bien été importé");
			    }

				    
    		}
    		$repondantModel = mvc_model('ApterRepondant');
    		$apterRepondants = $repondantModel->find([
    			'includes' => ['ApterQuestionnaire' => ['selects'=> 'ApterQuestionnaire.*'], 'ApterGroupe' => ['selects'=> 'ApterGroupe.*']],
    			'conditions' => [
    				'ApterRepondant.apter_questionnaire_id' => $id
    			]
    		]);

    		if( $apterRepondants != NULL){
    			$this->set('ApterRepondants', $apterRepondants);

    		}
    		
    	}
    }

    public function send_mail_repondant(){
    	if(!empty($this->params['id']) && !empty($this->params['repondant_id'])){
    		$id = $this->params['id'];
    		$repondant_id = $this->params['repondant_id'];
    		$repondantModel = mvc_model('ApterRepondant');

    		$value = $repondantModel->find_by_id($repondant_id);
    		$beneficiaire = $this->ApterQuestionnaire->find_by_id($id);

    		$mailModel = mvc_model('ApterMail');
    		$consultant = wp_get_current_user();
    		$mail = $mailModel->find_one_by_type('beneficiaire');


    		if($value->apter_groupe->abreviation != "VOUS"){
    				$to = $value->email;
					$subject = 'Apter - 360°Feed Back Leadership et Management';
					
					/*$body = mvc_render_to_string("apter_questionnaires/repondant_mail", ["consultant" => $consultant, "beneficiaire" => $beneficiaire, "id" => $this->params['id'], "repondant" => $value]);*/
					$mail = $mailModel->find_one_by_type('repondants');
					$body = $mail->mail;
					$body = str_replace("%REP_CIVILITE%",$value->civilite, $body);
					$body = str_replace("%REP_FIRSTNAME%",$value->firstname, $body);
					$body = str_replace("%REP_LASTNAME%",$value->lastname, $body);
					$body = str_replace("%BEN_CIVILITE%",$beneficiaire->ben_civilite, $body);
					$body = str_replace("%BEN_FIRSTNAME%",$beneficiaire->ben_firstname, $body);
					$body = str_replace("%BEN_LASTNAME%",$beneficiaire->ben_lastname, $body);
					$body = str_replace("%BEN_SOCIETE%",$beneficiaire->ben_societe, $body);
					$body = str_replace("%CONS_FIRSTNAME%",$consultant->user_firstname, $body);
					$body = str_replace("%CONS_LASTNAME%",$consultant->user_lastname, $body);
					$body = str_replace("%CONS_EMAIL%",$consultant->user_email, $body);
					$body = str_replace("%SITE_CODE%",$value->code, $body);
					$body = str_replace("%GROUPE_ABREV%",$value->apter_groupe->abreviation, $body);
					$body = str_replace("%GROUPE%",$value->apter_groupe->nom, $body);
					$body = str_replace("%SITE_LIEN%",htmlspecialchars( mvc_public_url(array('controller' => 'apter_questionnaires', 'action' => 'show', 'id' => $this->params['id'])) ), $body);

					$headers = array('Content-Type: text/html; charset=UTF-8');
					
					 
					wp_mail( $to, $subject, $body, $headers );
					$repondantModel->update($value->id, ['send' => 1]);
    			}else{
    				$repondantModel->update($value->id, ['send' => 1]);

    			}

    		echo "mail sended";

    		$url = MvcRouter::admin_url(array('controller' => 'admin_apter_questionnaires', 'action' => 'collecte', 'id' => $this->params['id']));
			$this->redirect($url);
    	}

    	die();
    }

    public function send_relance_mail_repondants(){
    	var_dump($this->params['repondants']);

    	if(!empty($this->params['id']) && !empty($this->params['repondants'])){
    		$ids = explode(',', $this->params['repondants']);
    		$id = $this->params['id'];

    		$repondants_list = "";
    		$i = 0; 
    		foreach ($ids as $key => $repondant_id) {
    			$repondantModel = mvc_model('ApterRepondant');

	    		$value = $repondantModel->find_by_id($repondant_id);
	    		$beneficiaire = $this->ApterQuestionnaire->find_by_id($id);

	    		$mailModel = mvc_model('ApterMail');
	    		$consultant = wp_get_current_user();
	    		$mail = $mailModel->find_one_by_type('beneficiaire');


    			if($value->apter_groupe->abreviation != "VOUS"){
    				$to = $value->email;
					$subject = 'Apter - 360°Feed Back Leadership et Management';
					
					/*$body = mvc_render_to_string("apter_questionnaires/repondant_mail", ["consultant" => $consultant, "beneficiaire" => $beneficiaire, "id" => $this->params['id'], "repondant" => $value]);*/
					$mail = $mailModel->find_one_by_type('relance');
					$body = $mail->mail;
					$body = str_replace("%REP_CIVILITE%",$value->civilite, $body);
					$body = str_replace("%REP_FIRSTNAME%",$value->firstname, $body);
					$body = str_replace("%REP_LASTNAME%",$value->lastname, $body);
					$body = str_replace("%BEN_CIVILITE%",$beneficiaire->ben_civilite, $body);
					$body = str_replace("%BEN_FIRSTNAME%",$beneficiaire->ben_firstname, $body);
					$body = str_replace("%BEN_LASTNAME%",$beneficiaire->ben_lastname, $body);
					$body = str_replace("%BEN_SOCIETE%",$beneficiaire->ben_societe, $body);
					$body = str_replace("%CONS_FIRSTNAME%",$consultant->user_firstname, $body);
					$body = str_replace("%CONS_LASTNAME%",$consultant->user_lastname, $body);
					$body = str_replace("%CONS_EMAIL%",$consultant->user_email, $body);
					$body = str_replace("%SITE_CODE%",$value->code, $body);
					$body = str_replace("%GROUPE_ABREV%",$value->apter_groupe->abreviation, $body);
					$body = str_replace("%GROUPE%",$value->apter_groupe->nom, $body);
					$body = str_replace("%SITE_LIEN%",htmlspecialchars(mvc_public_url(array('controller' => 'apter_questionnaires', 'action' => 'show', 'id' => $this->params['id']))), $body);

					$headers = array('Content-Type: text/html; charset=UTF-8');
					
					 
					wp_mail( $to, $subject, $body, $headers );
					$repondantModel->update($value->id, ['send' => 1]);

					$repondants_list .=$value->civilite." ".$value->firstname." ".$value->lastname;
					
					$i++;
					if(count($ids) != $i){
						$repondants_list.=" - ";
					}
				}else{
					$to = $value->email;
					$subject = 'Apter - 360°Feed Back Leadership et Management';
					
					/*$body = mvc_render_to_string("apter_questionnaires/repondant_mail", ["consultant" => $consultant, "beneficiaire" => $beneficiaire, "id" => $this->params['id'], "repondant" => $value]);*/
					$mail = $mailModel->find_one_by_type('relance_beneficiaire');
					$body = $mail->mail;
					$body = str_replace("%REP_CIVILITE%",$value->civilite, $body);
					$body = str_replace("%REP_FIRSTNAME%",$value->firstname, $body);
					$body = str_replace("%REP_LASTNAME%",$value->lastname, $body);
					$body = str_replace("%BEN_CIVILITE%",$beneficiaire->ben_civilite, $body);
					$body = str_replace("%BEN_FIRSTNAME%",$beneficiaire->ben_firstname, $body);
					$body = str_replace("%BEN_LASTNAME%",$beneficiaire->ben_lastname, $body);
					$body = str_replace("%BEN_SOCIETE%",$beneficiaire->ben_societe, $body);
					$body = str_replace("%CONS_FIRSTNAME%",$consultant->user_firstname, $body);
					$body = str_replace("%CONS_LASTNAME%",$consultant->user_lastname, $body);
					$body = str_replace("%CONS_EMAIL%",$consultant->user_email, $body);
					$body = str_replace("%SITE_CODE%",$value->code, $body);
					$body = str_replace("%GROUPE_ABREV%",$value->apter_groupe->abreviation, $body);
					$body = str_replace("%GROUPE%",$value->apter_groupe->nom, $body);
					$body = str_replace("%SITE_LIEN%",htmlspecialchars(mvc_public_url(array('controller' => 'apter_questionnaires', 'action' => 'show', 'id' => $this->params['id']))), $body);

					$headers = array('Content-Type: text/html; charset=UTF-8');
					
					 
					wp_mail( $to, $subject, $body, $headers );
					$repondantModel->update($value->id, ['send' => 1]);

					$repondants_list .=$value->civilite." ".$value->firstname." ".$value->lastname;
					
					$i++;
					if(count($ids) != $i){
						$repondants_list.=" - ";
					}

				}

					
    		}
    		$this->flash('notice',"Un mail de relance a été envoyé aux répondants suivants :  ".$repondants_list);
    	}

    	$url = MvcRouter::admin_url(array('controller' => 'admin_apter_questionnaires', 'action' => 'collecte', 'id' => $this->params['id']));
		$this->redirect($url);

    }

    public function send_relance_mail_repondant(){
    	if(!empty($this->params['id']) && !empty($this->params['repondant_id'])){
    		$id = $this->params['id'];
    		$repondant_id = $this->params['repondant_id'];
    		$repondantModel = mvc_model('ApterRepondant');

    		$value = $repondantModel->find_by_id($repondant_id);
    		$beneficiaire = $this->ApterQuestionnaire->find_by_id($id);

    		$mailModel = mvc_model('ApterMail');
    		$consultant = wp_get_current_user();
    		$mail = $mailModel->find_one_by_type('beneficiaire');


    		if($value->apter_groupe->abreviation != "VOUS"){
    				$to = $value->email;
					$subject = 'Apter - 360°Feed Back Leadership et Management';
					
					/*$body = mvc_render_to_string("apter_questionnaires/repondant_mail", ["consultant" => $consultant, "beneficiaire" => $beneficiaire, "id" => $this->params['id'], "repondant" => $value]);*/
					$mail = $mailModel->find_one_by_type('relance');
					$body = $mail->mail;
					$body = str_replace("%REP_CIVILITE%",$value->civilite, $body);
					$body = str_replace("%REP_FIRSTNAME%",$value->firstname, $body);
					$body = str_replace("%REP_LASTNAME%",$value->lastname, $body);
					$body = str_replace("%BEN_CIVILITE%",$beneficiaire->ben_civilite, $body);
					$body = str_replace("%BEN_FIRSTNAME%",$beneficiaire->ben_firstname, $body);
					$body = str_replace("%BEN_LASTNAME%",$beneficiaire->ben_lastname, $body);
					$body = str_replace("%BEN_SOCIETE%",$beneficiaire->ben_societe, $body);
					$body = str_replace("%CONS_FIRSTNAME%",$consultant->user_firstname, $body);
					$body = str_replace("%CONS_LASTNAME%",$consultant->user_lastname, $body);
					$body = str_replace("%CONS_EMAIL%",$consultant->user_email, $body);
					$body = str_replace("%SITE_CODE%",$value->code, $body);
					$body = str_replace("%GROUPE_ABREV%",$value->apter_groupe->abreviation, $body);
					$body = str_replace("%GROUPE%",$value->apter_groupe->nom, $body);
					$body = str_replace("%SITE_LIEN%",htmlspecialchars(mvc_public_url(array('controller' => 'apter_questionnaires', 'action' => 'show', 'id' => $this->params['id']))), $body);

					$headers = array('Content-Type: text/html; charset=UTF-8');
					
					 
					wp_mail( $to, $subject, $body, $headers );
					$repondantModel->update($value->id, ['send' => 1]);

					$this->flash('notice',"Un mail de relance a été envoyé à ".$value->civilite." ".$value->firstname." ".$value->lastname);
			}else{
					$to = $value->email;
					$subject = 'Apter - 360°Feed Back Leadership et Management';
					
					/*$body = mvc_render_to_string("apter_questionnaires/repondant_mail", ["consultant" => $consultant, "beneficiaire" => $beneficiaire, "id" => $this->params['id'], "repondant" => $value]);*/
					$mail = $mailModel->find_one_by_type('relance_beneficiaire');
					$body = $mail->mail;
					$body = str_replace("%REP_CIVILITE%",$value->civilite, $body);
					$body = str_replace("%REP_FIRSTNAME%",$value->firstname, $body);
					$body = str_replace("%REP_LASTNAME%",$value->lastname, $body);
					$body = str_replace("%BEN_CIVILITE%",$beneficiaire->ben_civilite, $body);
					$body = str_replace("%BEN_FIRSTNAME%",$beneficiaire->ben_firstname, $body);
					$body = str_replace("%BEN_LASTNAME%",$beneficiaire->ben_lastname, $body);
					$body = str_replace("%BEN_SOCIETE%",$beneficiaire->ben_societe, $body);
					$body = str_replace("%CONS_FIRSTNAME%",$consultant->user_firstname, $body);
					$body = str_replace("%CONS_LASTNAME%",$consultant->user_lastname, $body);
					$body = str_replace("%CONS_EMAIL%",$consultant->user_email, $body);
					$body = str_replace("%SITE_CODE%",$value->code, $body);
					$body = str_replace("%GROUPE_ABREV%",$value->apter_groupe->abreviation, $body);
					$body = str_replace("%GROUPE%",$value->apter_groupe->nom, $body);
					$body = str_replace("%SITE_LIEN%",htmlspecialchars(mvc_public_url(array('controller' => 'apter_questionnaires', 'action' => 'show', 'id' => $this->params['id']))), $body);

					$headers = array('Content-Type: text/html; charset=UTF-8');
					
					 
					wp_mail( $to, $subject, $body, $headers );
					$repondantModel->update($value->id, ['send' => 1]);

					$this->flash('notice',"Un mail de relance a été envoyé à ".$value->civilite." ".$value->firstname." ".$value->lastname);
    				
    		}	

    		$url = MvcRouter::admin_url(array('controller' => 'admin_apter_questionnaires', 'action' => 'collecte', 'id' => $this->params['id']));
			$this->redirect($url);

    	}

    	die();
    }

    public function send_mail(){
    	if(!empty($this->params['id'])){
    		$id = $this->params['id'];
    		$repondantModel = mvc_model('ApterRepondant');
    		$repondants = $repondantModel->find([
    			'includes' => ['ApterQuestionnaire' => ['selects'=> 'ApterQuestionnaire.*'], 'ApterGroupe' => ['selects'=> 'ApterGroupe.*']],
    			'conditions' => [
    				'ApterRepondant.apter_questionnaire_id' => $id
    			]
    		]);
    		$mailModel = mvc_model('ApterMail');
    		$consultant = wp_get_current_user();
			$beneficiaire = $this->ApterQuestionnaire->find_by_id($this->params['id']);
			$to = $beneficiaire->ben_email;
			$subject = 'Apter - 360°Feed Back Leadership et Management';
			$mail = $mailModel->find_one_by_type('beneficiaire');

			$body = $mail->mail;	
			$body = str_replace("%BEN_CIVILITE%",$beneficiaire->ben_civilite, $body);
			$body = str_replace("%BEN_FIRSTNAME%",$beneficiaire->ben_firstname, $body);
			$body = str_replace("%BEN_LASTNAME%",$beneficiaire->ben_lastname, $body);
			$body = str_replace("%CONS_FIRSTNAME%",$consultant->user_firstname, $body);
			$body = str_replace("%CONS_LASTNAME%",$consultant->user_lastname, $body);
			$body = str_replace("%CONS_EMAIL%",$consultant->user_email, $body);
			$body = str_replace("%SITE_CODE%",$beneficiaire->ben_code, $body);
			$body = str_replace("%GROUPE_ABREV%","VOUS", $body);
			$body = str_replace("%GROUPE%","Bénéficiaire", $body);
			$body = str_replace("%SITE_LIEN%",htmlspecialchars(mvc_public_url(array('controller' => 'apter_questionnaires', 'action' => 'show', 'id' => $this->params['id']))), $body);
			/*var_dump($body);
			die();*/
			$headers = array('Content-Type: text/html; charset=UTF-8');
			wp_mail( $to, $subject, $body, $headers );
			$this->ApterQuestionnaire->update($this->params['id'], ['send' => 1]);
			echo "ben sended";
			
    		foreach ($repondants as $key => $value) {
 				echo "repondant : ".$value->email;
    			if($value->apter_groupe->abreviation != "VOUS"){
    				$to = $value->email;
					$subject = 'Apter - 360°Feed Back Leadership et Management';
					
					/*$body = mvc_render_to_string("apter_questionnaires/repondant_mail", ["consultant" => $consultant, "beneficiaire" => $beneficiaire, "id" => $this->params['id'], "repondant" => $value]);*/
					$mail = $mailModel->find_one_by_type('repondants');
					$body = $mail->mail;
					$body = str_replace("%REP_CIVILITE%",$value->civilite, $body);
					$body = str_replace("%REP_FIRSTNAME%",$value->firstname, $body);
					$body = str_replace("%REP_LASTNAME%",$value->lastname, $body);
					$body = str_replace("%BEN_CIVILITE%",$beneficiaire->ben_civilite, $body);
					$body = str_replace("%BEN_FIRSTNAME%",$beneficiaire->ben_firstname, $body);
					$body = str_replace("%BEN_LASTNAME%",$beneficiaire->ben_lastname, $body);
					$body = str_replace("%BEN_SOCIETE%",$beneficiaire->ben_societe, $body);
					$body = str_replace("%CONS_FIRSTNAME%",$consultant->user_firstname, $body);
					$body = str_replace("%CONS_LASTNAME%",$consultant->user_lastname, $body);
					$body = str_replace("%CONS_EMAIL%",$consultant->user_email, $body);
					$body = str_replace("%SITE_CODE%",$value->code, $body);
					$body = str_replace("%GROUPE_ABREV%",$value->apter_groupe->abreviation, $body);
					$body = str_replace("%GROUPE%",$value->apter_groupe->nom, $body);
					$body = str_replace("%SITE_LIEN%",htmlspecialchars(mvc_public_url(array('controller' => 'apter_questionnaires', 'action' => 'show', 'id' => $this->params['id']))), $body);

					$headers = array('Content-Type: text/html; charset=UTF-8');
					
					 
					wp_mail( $to, $subject, $body, $headers );
					$repondantModel->update($value->id, ['send' => 1]);
    			}else{
    				$repondantModel->update($value->id, ['send' => 1]);

    			}
	    			
    		}
    		echo "repondnat sended";

    		$url = MvcRouter::admin_url(array('controller' => 'admin_apter_questionnaires', 'action' => 'collecte', 'id' => $this->params['id']));
			$this->redirect($url);
    			
    	}
    	die();

    }

    


    public function ajax_add_repondant(){
    	$reponse = [
    		'message' => "erreur lors de l'enregistrement"
    	];

    	$groupeModel = mvc_model('ApterGroupe');
    	$repondantModel = mvc_model('ApterRepondant');
    	if(!empty($this->params['data']['id']) && !empty($this->params['data']['ApterRepondant'])) {
    		$id = $this->params['data']['id'];
    		$value = $this->params['data']['ApterRepondant'];

    		$groupe = $groupeModel->find([
	    			"conditions" => [
	    				"ApterGroupe.apter_questionnaire_id" => $id,
	    				"ApterGroupe.abreviation" => $value['abrev']
	    			]
	    		]);
    		
    			if($groupe != NULL){
    				$groupe_id = $groupe[0]->id;
    			}else{
    				$this->params['data']['ApterGroupe'] = [
	    				"nom" => $value['groupe'],
	    				"abreviation" => $value['abrev'],
	    				"apter_questionnaire_id" => $id
	    			];
	    			$groupeModel->create($this->params['data']);
	    			$groupe_id = $groupeModel->insert_id;
    			}
    		
    			
    			$this->params['data']['ApterRepondant']['apter_questionnaire_id'] = $id;
    			$this->params['data']['ApterRepondant']['apter_groupe_id'] = $groupe_id;
    			$this->params['data']['ApterRepondant']['code'] = rand(1000000000,9999999999);
    			
    			
    			$repondantModel->create($this->params['data']);
    			$reponse['message'] = "Répondant ajouté avec succès";
    			$reponse['beneficiaire'] = $this->ApterQuestionnaire->find_by_id($id);
	    		$reponse['repondants'] =  $repondantModel->find([
	    			'includes' => ['ApterQuestionnaire' => ['selects'=> 'ApterQuestionnaire.*'], 'ApterGroupe' => ['selects'=> 'ApterGroupe.*']],
	    			'conditions' => [
	    				'ApterRepondant.apter_questionnaire_id' => $id
	    			]
	    		]);
    	}
    	
    	echo json_encode($reponse);
    	die();
    }


    public function ajax_add_repondants(){
    	$reponse = [
    		'message' => "erreur lors de l'enregistrement"
    	];
    	$groupeModel = mvc_model('ApterGroupe');
    	$repondantModel = mvc_model('ApterRepondant');

    	if(!empty($this->params['data']['id']) && !empty($this->params['data']['beneficiaire']) && !empty($this->params['data']['repondants'])) {
    		$id = $this->params['data']['id'];
    		$beneficiaire = $this->params['data']['beneficiaire'];
    		$beneficiaire['ben_code'] = rand(1000000000,9999999999);
    		$repondants = $this->params['data']['repondants'];
    		
    		/*$data = [
    			"ben_civilite" => isset($beneficiaire['ben_civilite']) ? $beneficiaire['ben_civilite'] : "",
    			"ben_firstname" => isset($beneficiaire['ben_firstname']) ? $beneficiaire['ben_firstname'] : "",
    			"ben_lastname" => isset($beneficiaire['ben_lastname']) ? $beneficiaire['ben_lastname'] : "",
    			"ben_initiales" => isset($beneficiaire['ben_initiales']) ? $beneficiaire['ben_initiales'] : "",
    			"ben_email" => isset($beneficiaire['ben_email']) ? $beneficiaire['ben_email'] : "",
    			"ben_tel" => isset($beneficiaire['ben_tel']) ? $beneficiaire['ben_tel'] : "",
    			"ben_societe" => isset($beneficiaire['ben_tel']) ? $beneficiaire['ben_societe'] : "",
    			"ben_code" => rand(1000000000,9999999999)
    		];*/
    		
    		$this->ApterQuestionnaire->update($id, $beneficiaire);

    		$reps = $repondantModel->find_by_apter_questionnaire_id($id);
    		foreach ($reps as $key => $value) {
    			$repondantModel->delete($value->id);
    		}

    		$groupe = $groupeModel->find([
    			"conditions" => [
    				"ApterGroupe.apter_questionnaire_id" => $id,
    				"ApterGroupe.abreviation" => "VOUS"
    			]
    		]);
    			if($groupe != NULL){
    				$groupe_id = $groupe[0]->id;
    			}else{
    				$this->params['data']['ApterGroupe'] = [
	    				"nom" => "Bénéficiaire",
	    				"abreviation" => "VOUS",
	    				"apter_questionnaire_id" => $id
	    			];
	    			$groupeModel->create($this->params['data']);
	    			$groupe_id = $groupeModel->insert_id;
    			}

    			$this->params['data']['ApterRepondant'] = [
    				"email" => $beneficiaire['ben_email'],
    				"firstname" => $beneficiaire['ben_firstname'],
    				"lastname" => $beneficiaire['ben_lastname'],
    				"civilite" => $beneficiaire['ben_civilite']
    			];
    			$this->params['data']['ApterRepondant']['apter_questionnaire_id'] = $id;
    			$this->params['data']['ApterRepondant']['apter_groupe_id'] = $groupe_id;
    			$this->params['data']['ApterRepondant']['code'] = $beneficiaire['ben_code'];
    			
    			
    			$repondantModel->create($this->params['data']);
    			$ben_id = $repondantModel->insert_id;



    		
    		$groupes = $groupeModel->find_by_apter_questionnaire_id($id);
    		
    		

    		foreach ($repondants as $key => $value) {
    			
    			$groupe = $groupeModel->find([
	    			"conditions" => [
	    				"ApterGroupe.apter_questionnaire_id" => $id,
	    				"ApterGroupe.abreviation" => $value['abrev']
	    			]
	    		]);
    			if($groupe != NULL){
    				$groupe_id = $groupe[0]->id;
    			}else{
    				$this->params['data']['ApterGroupe'] = [
	    				"nom" => $value['groupe'],
	    				"abreviation" => $value['abrev'],
	    				"apter_questionnaire_id" => $id
	    			];
	    			$groupeModel->create($this->params['data']);
	    			$groupe_id = $groupeModel->insert_id;
    			}

    			$this->params['data']['ApterRepondant'] = $value;
    			$this->params['data']['ApterRepondant']['apter_questionnaire_id'] = $id;
    			$this->params['data']['ApterRepondant']['apter_groupe_id'] = $groupe_id;
    			$this->params['data']['ApterRepondant']['code'] = rand(1000000000,9999999999);
    			
    			
    			$repondantModel->create($this->params['data']);
    			$reponse['message'] = "Répondants ajoutés avec succès";
    		}

    		$reponse['beneficiaire'] = $this->ApterQuestionnaire->find_by_id($id);
    		$reponse['repondants'] =  $repondantModel->find([
    			'includes' => ['ApterQuestionnaire' => ['selects'=> 'ApterQuestionnaire.*'], 'ApterGroupe' => ['selects'=> 'ApterGroupe.*']],
    			'conditions' => [
    				'ApterRepondant.apter_questionnaire_id' => $id
    			]
    		]);

    	}
    	echo json_encode($reponse);
    	die();
    }

    public function add() {

	    if (!empty($this->params['data']) && !empty($this->params['data']['ApterQuestionnaire'])) {
	      $object = $this->params['data']['ApterQuestionnaire'];
	      $this->params['data']['ApterQuestionnaire']['consultant_id'] = get_current_user_id();
	      if (empty($object['id'])) {
	        $this->ApterQuestionnaire->create($this->params['data']);
	        $id = $this->ApterQuestionnaire->insert_id;
	        $url = MvcRouter::admin_url(array('controller' => 'admin_apter_pages', 'action' => 'add', 'quest_id' => $id));
	        $this->flash('notice', 'Successfully created!');
	        $this->redirect($url."&quest_id=".$id);
	      }
	    }
	    $objects = $this->ApterQuestionnaire->find();
	    $this->set('objects', $objects);
	 }

	 public function duplicate(){
	 	if(!empty($this->params['data'])){
	 		echo "here";
	 		$pageModel = mvc_model('ApterPage');
	 		$questionModel = mvc_model('ApterQuestion');
	 		$reponseModel = mvc_model('ApterReponse');
	 		$id = $this->params['data'];
	 		$object = 	$this->ApterQuestionnaire->find_by_id($id, array(
    			'includes' => array('ApterPage' => ['includes' => ['ApterQuestion' => ['selects' => 'ApterQuestion.*']],'selects' => 'ApterPage.*'])
	    	));
	    	$data = [];
	 		$object->type = "Copie de ".$object->type;

	 		$data["ApterQuestionnaire"] = [
	 			"type" => $object->type,
	 			"consultant_id" => get_current_user_id()

	 		];

	 		$this->ApterQuestionnaire->create($data);
	 		$id = $this->ApterQuestionnaire->insert_id;
	 		foreach ($object->apter_pages as $key => $value) {
	 			
	 			$data['ApterPage'] = [
	 				"apter_questionnaire_id" => $id,
	 				"titre" => $value->titre,
	 				"sous_titre" => $value->sous_titre
	 			];
	 			$pageModel->create($data);
	 			$page_id = $pageModel->insert_id;
	 			foreach ($value->apter_questions as $key2 => $question) {
	 				
	 				
	 				$data['ApterQuestion'] = [
	 					"apter_questionnaire_id" => $id,
	 					"apter_page_id" => $page_id,
	 					"question" => $question->question,
	 					"type" => $question->type,
	 					"negative" => $question->negative
	 				];
	 				$questionModel->create($data);
	 				$question_id = $questionModel->insert_id;
	 				foreach ($question->apter_reponses as $key3 => $reponse) {
	 					
	 					$data["ApterReponse"] = [
	 						"apter_question_id" => $question_id,
	 						"reponse" => $reponse->reponse,
	 						"score" => $reponse->score
	 					];
	 					$reponseModel->create($data);
	 				}
	 			}


	 		}
	 		

	 		$url = MvcRouter::admin_url(array('controller' => 'admin_apter_questionnaires', 'action' => 'edit', "id" => $id));
			$this->redirect($url);



	 	}
	 	$url = MvcRouter::admin_url(array('controller' => 'admin_apter_questionnaires', 'action' => 'add'));
		$this->redirect($url);
	 }

	 public function ajax_add() {

	    if (!empty($this->params['data'])) {
	      $object = $this->params['data'];
	      $data = [];
	      $data['ApterQuestionnaire'] = $this->params['data'];

	      if (empty($object['id'])) {
	      	$data['ApterQuestionnaire']['consultant_id'] = get_current_user_id();
	        $this->ApterQuestionnaire->create($data);
	        $id = $this->ApterQuestionnaire->insert_id;
	       	echo $id;
	 
	      }else{
	      	$this->ApterQuestionnaire->update($object['id'], array('type' => $object["type"]));
	      	echo $object['id'];
	      	
	      }
	    }
	    // To do: add JSON output confirming sucess
		die();
	 }

	 public function ajax_page_title() {
	 	$pageModel = mvc_model('ApterPage');

	    if(!empty($this->params['data']['ApterPage']) && !empty($this->params['data']['ApterQuestionnaire']['id'])){

	      	 	$page = $this->params['data']['ApterPage'];
	      	 	$quest_id = $this->params['data']['ApterQuestionnaire']['id'];
	      	 	
	      	 	if(empty($page['id'])){
	      	 		
	      	 		$this->params['data']['ApterPage']['apter_questionnaire_id'] = $quest_id;
	      	 		$pageModel->create($this->params['data']);
	      	 		$id = $pageModel->insert_id;
	      	 		echo $id;
	      	 	}else{
	      	 		
	      	 		$titre = isset($page['titre']) ? $page['titre'] : "" ;
	      	 		$sous_titre = isset($page['sous_titre']) ? $page['sous_titre'] : "" ;
	      	 		$description = isset($page['description']) ? $page['description'] : "" ;
	      	 		$data = ['titre' => $titre, "sous_titre" => $sous_titre, "description" => $description];
	      	 		$pageModel->update($page['id'],$data);
	      	 		echo $page['id'];
	      	 	}
	    }
	    // To do: add JSON output confirming sucess
		die();
	 }

	 public function ajax_add_question(){
	 	$questionModel =mvc_model('ApterQuestion');
	 	$pageModel =mvc_model('ApterPage');
	 	$reponseModel =mvc_model('ApterReponse');
	 	$reponse = [
	 		'message' => "Erreur lors de l'enregistrement!"
	 	];
	 	/*$pages = $pageModel->find_by_apter_questionnaire_id(37);
	 	var_dump($pages);*/
	 	if(!empty($this->params['data']['ApterQuestion'])){
	 		$question = $this->params['data']['ApterQuestion'];
	 		if(empty($question['apter_page_id'])){
	 			$pageModel->create($this->params['data']);
	 			$question['apter_page_id'] = $pageModel->insert_id;
	 			$reponse['apter_page_id'] = $question['apter_page_id'];
	 			$this->params['data']['ApterQuestion'] = $question;
	 		}else{
	 			$reponse['apter_page_id'] = $question['apter_page_id'];
	 		}
	 		if(empty($question['id'])){
	 			$questionModel->create($this->params['data']);
	 			$id =  $questionModel->insert_id;
	 			foreach ($question['reponses'] as $key => $value) {
	 				$value['apter_question_id'] = $id;
	 				$this->params['data']['ApterQuestion']['ApterReponse'] = $value;
	 				$reponseModel->create($this->params['data']['ApterQuestion']);
	 			}
	 			$reponse['id'] = $id;
	 			$reponse['message'] = "La question a été créée";
	 			
	 			
	 		}else{
	 			$data = ["question" => $question['question'],  'type' => $question['type'], "negative" => $question['negative']];
	 			$questionModel->update($question['id'], $data);

	 			//deleting all reponse of this question
	 			$reponses = $reponseModel->find_by_apter_question_id($question['id']);
	 			foreach ($reponses as $key => $value) {
	 				$reponseModel->delete($value->id);
	 			}
	 			foreach ($question['reponses'] as $key => $value) {
	 				$value['apter_question_id'] = $question['id'];
	 				$this->params['data']['ApterQuestion']['ApterReponse'] = $value;
	 				$reponseModel->create($this->params['data']['ApterQuestion']);
	 			}
	 			$reponse['id'] = $question['id'];
	 			$reponse['message'] = "La question a été mis à jour";
	 		}

	 	}
	 	echo json_encode($reponse);

	 	die();
	 }

	 public function ajax_delete_question(){
	 	if(!empty($this->params['data'])){
	 		$questionModel = mvc_model('ApterQuestion');
	 		$questionModel->delete($this->params['data']);
	 	}
	 	die();
	 }

	  public function ajax_delete_page(){
	 	if(!empty($this->params['data'])){
	 		$pageModel = mvc_model('ApterPage');
	 		$pageModel->delete($this->params['data']);
	 	}
	 	die();
	 }

	 public function ajax_add_reponse(){
	 	$reponseModel =mvc_model('ApterReponse');

	 	if(!empty($this->params['data']['ApterReponse'])){
	 		$reponse = $this->params['data']['ApterReponse'];
	 		if(empty($reponse['id'])){
	 			$reponseModel->create($this->params['data']);
	 			echo $reponseModel->insert_id;
	 		}else{
	 			$data = ["reponse" => $reponse['reponse'], 'score' => isset($reponse['score']) ? $reponse['score'] : 1];
	 			$reponseModel->update($reponse['id'], $data);
	 			echo $reponse['id'];

	 		}
	 	}

	 }
    
}

?>