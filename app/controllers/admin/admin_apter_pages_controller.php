<?php

class AdminApterPagesController extends MvcAdminController {
    
    var $default_columns = array('id', 'titre', 'sous_titre');

    public function add() {
   		if(isset($_GET['quest_id'])){
   			$quest_id = $_GET['quest_id'];
   			$quest = mvc_model('ApterQuestionnaire')->find_by_id($quest_id);
   			
   		}else{

   		}
	    if (!empty($this->params['data']) && !empty($this->params['data']['ApterPage'])) {
	      $object = $this->params['data']['ApterPage'];
	      if (empty($object['id'])) {
	        $this->ApterPage->create($this->params['data']);
	        $id = $this->ApterPage->insert_id;
	        $url = MvcRouter::admin_url(array('controller' => $this->name, 'action' => 'edit'));
	        $this->flash('notice', 'Successfully created!');
	        $this->redirect($url);
	      }
	    }
	 }
    
}

?>