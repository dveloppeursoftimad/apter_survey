<?php

class AdminApterCcsController extends MvcAdminController {
    
    var $default_columns = array('id', 'name');
    
    function index(){
        $this->init_default_columns();
        $this->process_params_for_search();
        $this->params['order'] = 'date_add DESC';
        $this->load_model('ApterCc');
        $Apter_cc = $this->ApterCc->paginate($this->params);  
        $this->set('Apter_cc', $Apter_cc['objects']); 
        $this->set_pagination($Apter_cc);     
    }

    function ajouterConsultant(){
        date_default_timezone_set('Europe/Paris');
        global $wpdb;
        $servername = DB_HOST;
        $username = DB_USER;
        $password = DB_PASSWORD;
        $dbname = DB_NAME;

        $nom = $_POST["nom"];
        $prenom = $_POST["prenom"];
        $email = $_POST["email"];
        $phone  = $_POST["phone"];
        $organisation = $_POST["organisation"];
        $exe = $wpdb->prefix ;
        $date_add = date("Y-m-d H:i:s");
    
        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        } 

        $sql = "INSERT INTO " .$wpdb->prefix."apter_ccs"." (nom,prenom,email,phone,organisation,date_add)
        VALUES ('$nom', '$prenom', '$email','$phone','$organisation','$date_add')";

        if ($conn->query($sql) === TRUE) {
            echo "New record created successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }

        $conn->close();
        $url = MvcRouter::admin_url(array('controller' => 'apter_ccs','action' => 'index'));
		$this->redirect($url);;
        //$this->set('exe', $exe);
    }

    function modifierConsultant(){
        global $wpdb;
        $servername = DB_HOST;
        $username = DB_USER;
        $password = DB_PASSWORD;
        $dbname = DB_NAME;

        $nom = $_POST["nom"];
        $prenom = $_POST["prenom"];
        $email = $_POST["email"];
        $phone  = $_POST["phone"];
        $organisation = $_POST["organisation"];
        $id = $_POST["id"]; 
    
        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        } 

        $sql = "INSERT INTO " .$wpdb->prefix."apter_ccs"." (nom,prenom,email,phone,organisation)
        VALUES ('$nom', '$prenom', '$email','$phone','$organisation')";
        $sql = "UPDATE ".$wpdb->prefix."apter_ccs SET nom = '$nom', prenom = '$prenom', email ='$email' , phone = '$phone', organisation = '$organisation ' WHERE id = $id ";

        if ($conn->query($sql) === TRUE) {
            echo "Updated successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }

        $conn->close();
        
        $url = MvcRouter::admin_url(array('controller' => 'apter_ccs','action' => 'index'));
		$this->redirect($url);;
    }


    function modifierPassword(){
        global $wpdb;
        $servername = DB_HOST;
        $username = DB_USER;
        $password = DB_PASSWORD;
        $dbname = DB_NAME;
        $id = $_POST["id"];
        $passworda = $_POST["password"];
        
        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        } 

        $sql = "UPDATE ".$wpdb->prefix."apter_ccs SET password = $passworda WHERE id = $id ";

        if ($conn->query($sql) === TRUE) {
            echo "updated successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }

        $conn->close();
        
        $url = MvcRouter::admin_url(array('controller' => 'apter_ccs','action' => 'index'));
		$this->redirect($url);;
    }

    public function delete(){
        

        $id = $_GET['id'];

        global $wpdb;
        $servername = DB_HOST;
        $username = DB_USER;
        $password = DB_PASSWORD;
        $dbname = DB_NAME;

        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        } 
        $sql = "SET FOREIGN_KEY_CHECKS=0";
        if ($conn->query($sql) === TRUE){
            echo "Set Correctly";    
        }

        $aa = $wpdb->prefix."apter_ccs";
        $ab = $wpdb->prefix."apter_questionnaires";
        $ac = $wpdb->prefix."apter_questions";
        $ad = $wpdb->prefix."apter_repondants";
        $ae = $wpdb->prefix."apter_pages";
        $af = $wpdb->prefix."apter_groupes";
        $ag = $wpdb->prefix."apter_reponses";

        $sql = "DELETE 
        Co,
        Qu,
        Qe,
        Re,
        Pa,
        Gr,
        Reply
    FROM
        $aa Co
    LEFT JOIN
        $ab Qu ON Co.id = Qu.consultant_id
    LEFT JOIN $ac Qe ON Qu.id = Qe.apter_questionnaire_id
    LEFT JOIN $ad Re ON Qu.id = Re.apter_questionnaire_id
    LEFT JOIN $ae Pa ON Qu.id = Pa.apter_questionnaire_id
    LEFT JOIN $af Gr ON Qu.id = Gr.apter_questionnaire_id
    LEFT JOIN $ag Reply ON Qe.id = Reply.apter_question_id
        WHERE  Co.id=$id  ";
        


    if ($conn->query($sql) === TRUE) {
        echo "Succes Delete of all correspondant table ";
        $this->flash('notice', 'Suppression r�ussi!');
    } else {
        echo "Error deleting record: " . $conn->error;
    }

    $conn->close();
    $url = MvcRouter::admin_url(array('controller' => 'apter_ccs', 'action' => 'index'));
	$this->redirect($url);
    }


}

?>
