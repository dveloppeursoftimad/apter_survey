<?php

class AdminApterRepondantsController extends MvcAdminController {
    
    var $default_columns = array('id','firstname','lastname', 'reponse');

    function index(){
        /*
        $this->init_default_columns();
        $this->process_params_for_search();
        $this->params['order'] = 'firstname ASC';
        $this->load_model('ApterRepondant');
        $Apter_repondant = $this->ApterRepondant->paginate($this->params);  
        $this->set('Apter_repondant', $Apter_repondant['objects']); 
        $this->set_pagination($Apter_repondant); */

        global $wpdb;
        $servername = DB_HOST;
        $username = DB_USER;
        $password = DB_PASSWORD;
        $dbname = DB_NAME;

        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        } 
        $a = $wpdb->prefix."apter_repondants";
        $b = $wpdb->prefix."apter_questionnaires";
        $c = $wpdb->prefix."apter_ccs";
        $sql = "SELECT 
        Re.lastname,
        Re.firstname,
        Re.id,
        Qu.ben_langue,
        Co.nom,
        Co.prenom
    FROM
        $a Re
    INNER JOIN
        $b Qu ON Re.apter_questionnaire_id = Qu.id
    INNER JOIN
        $c Co ON Qu.consultant_id = Co.id ";
        
        $result = $conn->query($sql);    
        $conn->close();    
        $this->set('Apter_repondant', $result);
    /*
        $table = 'SELECT 
        Re.lastname,
        Qu.consultant_id,
        Co.nom
    FROM
        apter_apter_repondants Re
    INNER JOIN
        apter_apter_questionnaires Qu ON Re.apter_questionnaire_id = Qu.id
    INNER JOIN
        apter_apter_ccs Co ON Qu.consultant_id = Co.id ';


    $primaryKey = 'id';

    $columns = array(
    array( 'db' => 'lastname',          'dt' => 0 ),
    array( 'db' => 'consultant_id',        'dt' => 1 ),
    array( 'db' => 'nom',    'dt' => 2 )
    );
    
    $sql_details = array(
    'user' => DB_USER,
    'pass' => DB_PASSWORD,
    'db'   => DB_NAME,
    'host' => DB_HOST
    );

    require_once( 'ssp.class.php' );
    echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
    );
    */    
        



    }

    public function delete(){
        /*
        $id = $_GET['id'];

        global $wpdb;
        $servername = DB_HOST;
        $username = DB_USER;
        $password = DB_PASSWORD;
        $dbname = DB_NAME;

        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        } 

        $sql = "DELETE 
        Re,
        Qu
    FROM
        apter_apter_repondants Re
    INNER JOIN
        apter_apter_questionnaires Qu ON Re.apter_questionnaire_id = Qu.id
        WHERE  Re.id=$id ";


        if ($conn->query($sql) === TRUE) {
            echo "Record deleted successfully";
        } else {
            echo "Error deleting record: " . $conn->error;
        }

        if ($conn->query($sql) === TRUE) {
            echo "Record deleted successfully";
        } else {
            echo "Error deleting record: " . $conn->error;
        }

        $conn->close();
        */
        $this->set_object();
        $this->load_model('ApterRepondant');
	    if (!empty($this->object) && !empty($this->params['id'])) {
	      $this->ApterRepondant->delete($this->params['id']);
	      $this->flash('notice', 'Suppression réussi!');
	    } else {
	      $this->flash('warning', 'Le test choisi n\'existe pas.');
	    }    		
		$url = MvcRouter::admin_url(array('controller' => 'apter_repondants', 'action' => 'index'));
		$this->redirect($url);
    }

    function test (){

        $this->init_default_columns();
        $this->process_params_for_search();
        $this->params['order'] = 'id ASC';
        $this->load_model('ApterRepondant');
        $Apter_repondant = $this->ApterRepondant->find($this->params);
        $response = $Apter_repondant;
        $this->set('Apter_repondant', $Apter_repondant['objects']); 
        //$this->set_pagination($Apter_repondant);  
       
        global $wpdb;
        $servername = DB_HOST;
        $username = DB_USER;
        $password = DB_PASSWORD;
        $dbname = DB_NAME;

        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        } 

        $sql = "SELECT 
        Re.lastname,
        Qu.consultant_id,
           Co.nom
    FROM
        apter_apter_repondants Re
    INNER JOIN
        apter_apter_questionnaires Qu ON Re.apter_questionnaire_id = Qu.id
    INNER JOIN
        apter_apter_ccs Co ON Qu.consultant_id = Co.id ";
        
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            // output data of each row
            $row = $result->fetch_assoc();
            $this->set('result', $result);
        }
       
    }
    
    
}

?>