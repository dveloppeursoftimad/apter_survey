<?php
MvcRouter::public_connect('{:controller}', array('action' => 'index'));
MvcRouter::public_connect('{:controller}/{:id:[\d]+}', array('action' => 'show'));
MvcRouter::public_connect('{:controller}/{:action}/{:id:[\d]+}');


MvcRouter::admin_ajax_connect(array('controller' => 'admin_apter_questionnaires', 'action' => 'ajax_add'));
MvcRouter::admin_ajax_connect(array('controller' => 'admin_apter_questionnaires', 'action' => 'ajax_page_title'));
MvcRouter::admin_ajax_connect(array('controller' => 'admin_apter_questionnaires', 'action' => 'ajax_add_question'));

MvcRouter::admin_ajax_connect(array('controller' => 'admin_apter_questionnaires', 'action' => 'ajax_add_repondants'));
MvcRouter::admin_ajax_connect(array('controller' => 'admin_apter_questionnaires', 'action' => 'ajax_add_repondant'));
MvcRouter::admin_ajax_connect(array('controller' => 'admin_apter_questionnaires', 'action' => 'ajax_validate_repondant'));
MvcRouter::admin_ajax_connect(array('controller' => 'admin_apter_questionnaires', 'action' => 'ajax_add_result'));
MvcRouter::admin_ajax_connect(array('controller' => 'admin_apter_questionnaires', 'action' => 'ajax_delete_question'));
MvcRouter::admin_ajax_connect(array('controller' => 'admin_apter_questionnaires', 'action' => 'ajax_delete_page'));
MvcRouter::admin_ajax_connect(array('controller' => 'admin_apter_questionnaires', 'action' => 'ajax_export'));

?>