<?php

/*var_dump(MvcConfiguration::get_instance());*/

MvcConfiguration::append(array(
  'AdminPages' => array(
    'apter_pages' => array(
      'add',
      'delete',
      'edit'
    ),
    'apter_questionnaires' => array(
      'label' => 'Apter 360 FB',
      'add' => ['label' => 'Créer un questionnaire'],
      'delete',
      'edit',
      'matrice'=> ['label' => 'Matrice de rapport'],
      'supprimer' => ['in_menu' => false],
      'collecte' => ['in_menu' => false],
      'send_mail' => ['in_menu' => false],
      'send_mail_repondant' => ['in_menu' => false],
      'send_relance_mail_repondant' => ['in_menu' => false],
      'send_relance_mail_repondants' => ['in_menu' => false],
      'analyse' => ['in_menu' => false],
      'export' => ['in_menu' => false],
      'duplicate' => ['in_menu' => false],
      'reponses' => ['in_menu' => false],
      /*'ajax_add' => array(
            'in_menu' => false
      )*/
    ),
    'apter_repondants' => array(
        'add' => ['in_menu' => false],
        'delete'=>['in_menu' => false]
    ),
    'apter_mails' => array(
      'label' => "Apter Mails 360 FB",
      'add' => ['in_menu' => false],
      'delete',
      'edit'
    ),
    'apter_ccs' => array(
      'add' => ['in_menu' => false],
      'ajouterConsultant' => ['in_menu' => false],
      'modifierPassword' => ['in_menu' => false],
      'modifierConsultant' => ['in_menu' => false],
      'delete' => ['in_menu' => false]
    )
  )
));


add_action('wp_head', 'apter_plugin_ajaxurl');

function apter_plugin_ajaxurl() {

   echo '<script type="text/javascript">
           var ajaxurl = "' . admin_url('admin-ajax.php') . '";
         </script>';
}

add_action( 'admin_init', 'apter_remove_menus' );

function apter_remove_menus(){
  global $menu;
  global $submenu;

  add_submenu_page("mvc_apter_questionnaires", "Apter Mails 360 FB", "Apter Mails 360 FB", 'administrator', 'admin.php?page=mvc_apter_mails');
  add_submenu_page("mvc_apter_questionnaires", "Consultants", "Consultants", 'administrator', 'admin.php?page=mvc_apter_ccs');
  add_submenu_page("mvc_apter_questionnaires", "Coaché", "Coaché", 'administrator', 'admin.php?page=mvc_apter_repondants');
  remove_menu_page('mvc_apter_pages');
  remove_menu_page('mvc_apter_ccs');
  remove_menu_page('mvc_apter_repondants');
  remove_menu_page('mvc_apter_mails');
}

?>